There are 7 different statistics that specialize your character:
1. STA (Stamina) - determines your max health
2. STR (Strength) - increases physical attack damage
3. DEX (Dexterity) - ignores defence if you have more than your foe
4. DEF (Defence) - decreases the damage taken
5. INT (Intelligence) - increases skill damage
6. MAT (Mastery) - increases critical hit chance
7. CHA (Charisma) - increases critical hit damage
Every race scales with different stats.
See Race Info for more info on that.
You level up whenever you get enough xp. You can get xp from killing enemies.
Each time you level up you get a stat point. You can use said points to increase your stats.
Each certain amount of levels you will evolve.
Sometimes you will have the option to choose your path.
Whenever you evolve your stats will increase according to your evolution.
You will also get skills for your affiliating race. Each race has its own preferred stats.
Current race info can be seen in Race Info.
Whenever you collide with an enemy or boss, you will enter combat with them.
While in combat you will be able to see your stats and current health on the left 
and your opponent's on the right. Next to them a list of all your skills will be displayed.
Clicking on any of them will perform said skill and put it on cooldown for 1 round.
After you attack, your enemy will damage you back if it has not died.
When you kill an opponent you will be taken back to the map and continue your adventure 
further. Killing an opponent rewards you with xp. When you defeat a boss,
you will be transported to the next floor.