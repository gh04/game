package entity;

import java.awt.image.BufferedImage;
import java.io.Serializable;

public abstract class Entity implements Serializable {
	// position
	private int posx = 0;
	private int posy = 0;
	// stats
	private int stamina = 1;
	private int strength = 1;
	private int dexterity = 1;
	private int defence = 1;
	private int intelligence = 1;
	private int mastery = 1;
	private int charisma = 1;
	private int maxHealth = 5 + stamina;
	private int curHealth = maxHealth;
	//image
	transient private BufferedImage img;

	public int getPosX() {
		return posx;
	}

	public void setPosX(int x) {
		posx = x;
	}

	public int getPosY() {
		return posy;
	}

	public void setPosY(int y) {
		posy = y;
	}

	// position
	public int getSTA() {
		return stamina;
	}

	public void setSTA(int s) {
		stamina = s;
	}

	public int getSTR() {
		return strength;
	}

	public void setSTR(int s) {
		strength = s;
	}

	public int getDEX() {
		return dexterity;
	}

	public void setDEX(int d) {
		dexterity = d;
	}

	public int getDEF() {
		return defence;
	}

	public void setDEF(int d) {
		defence = d;
	}

	public int getINT() {
		return intelligence;
	}

	public void setINT(int i) {
		intelligence = i;
	}

	public int getMAS() {
		return mastery;
	}

	public void setMAS(int m) {
		mastery = m;
	}

	public int getCHA() {
		return charisma;
	}

	public void setCHA(int c) {
		charisma = c;
	}

	public int getMaxHP() {
		return maxHealth;
	}

	public void setMaxHP(int h) {
		maxHealth = h;
	}

	public int getCurrentHP() {
		return curHealth;
	}

	public void setCurrentHP(int h) {
		curHealth = h;
	}
	// stats

	public BufferedImage getImg() {
		return img;
	}

	public void setImg(BufferedImage i) {
		img = i;
	}
	//img
}
