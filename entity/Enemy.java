package entity;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import javax.imageio.ImageIO;

public class Enemy extends Entity implements Serializable {
	private String imgFile;

	public Enemy(int x, int y, int f) {
		Random r = new Random();

		this.setPosX(x);
		this.setPosY(y);
		this.setSTA(r.nextInt(1, 2 * f));
		this.setSTR(r.nextInt(1, 2 * f));
		this.setDEX(r.nextInt(1, 2 * f));
		this.setDEF(r.nextInt(1, 2 * f));
		this.setINT(r.nextInt(1, 2 * f));
		this.setMAS(r.nextInt(1, 2 * f));
		this.setCHA(r.nextInt(1, 2 * f));
		this.setMaxHP(5 + this.getSTA());
		this.setCurrentHP(this.getMaxHP());

		try {
			this.setImgFile("art/minis/enemy/enemy" + r.nextInt(1, 4) + ".png");
			this.setImg(ImageIO.read(new File(this.getImgFile())));
		} catch (IOException e) {
			this.setImg(null);
		}
	}

	public Enemy(int x, int y) {
		this.setPosX(x);
		this.setPosY(y);
	}

	public String getImgFile() {
		return imgFile;
	}

	public void setImgFile(String i) {
		imgFile = i;
	}
	// img file
}
