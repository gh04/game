package entity;

import frame.*;

import java.awt.Button;
import java.awt.Color;
import java.io.BufferedReader;
import java.util.*;

public class Combat {
	private int damage;
	GameFrame frame;
	public BufferedReader sc;
	Random r = new Random();
	private String skill = "";
	private List<Enemy> enemies;
	private int[] tempStats = { 0, 0, 0, 0, 0, 0, 0 };
	private Boss boss;
	private int pos;
	private boolean isUsed = false;

	public Combat(GameFrame f) {
		frame = f;
	}

	public synchronized void enemyFight() {
		damage = useSkill(skill, "enemy")
				- ((getEnemies().get(getPos()).getDEF() - (frame.floorPanel.floor.hero.getDEX() + tempStats[2])) >= 0
						? getEnemies().get(getPos()).getDEF() - (frame.floorPanel.floor.hero.getDEX() + tempStats[2])
						: 0);

		if (damage < 0)
			damage = 0;
		getEnemies().get(getPos()).setCurrentHP(getEnemies().get(getPos()).getCurrentHP() - damage);
		System.out.println("You used " + skill + " and dealt " + damage + " damage.");

		if (getEnemies().get(getPos()).getCurrentHP() <= 0) {
			for (int i = 0; i < tempStats.length; i++) {
				tempStats[i] = 0;
			}

			isUsed = false;
			frame.combatPanel.setVisible(false);
			frame.floorPanel.setVisible(true);
			frame.textPanel.clearButtons();
			frame.textPanel.infoSwitch.setVisible(true);
			frame.floorPanel.floor.hero
					.setXP(frame.floorPanel.floor.hero.getXP() + 10 * frame.floorPanel.floor.getFloor());
			frame.floorPanel.floor.hero.lvlUP();
			if (frame.floorPanel.floor.hero.getLevel() == 5 || frame.floorPanel.floor.hero.getLevel() == 10
					|| frame.floorPanel.floor.hero.getLevel() == 20 || frame.floorPanel.floor.hero.getLevel() == 30) {
				frame.floorPanel.floor.hero.changeImage();
			}
			getEnemies().remove(getPos());
			if (!frame.textPanel.heading.getText().equals("EVOLVE")) {
				frame.command.readCommand("info");
			}
		}

		else {
			int crit = r.nextInt(0, 100);
			if (crit <= getEnemies().get(getPos()).getMAS()) {

				try {
					frame.combatPanel.hero.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.hero.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((getEnemies().get(getPos()).getSTR()
						* (1.5 + 0.01 * getEnemies().get(getPos()).getCHA()))
						- (((frame.floorPanel.floor.hero.getDEF() + tempStats[3])
								- getEnemies().get(getPos()).getDEX()) >= 0
										? (frame.floorPanel.floor.hero.getDEF() + tempStats[3])
												- getEnemies().get(getPos()).getDEX()
										: 0));
			} else {
				damage = getEnemies().get(getPos()).getSTR() - (((frame.floorPanel.floor.hero.getDEF() + tempStats[3])
						- getEnemies().get(getPos()).getDEX()) >= 0
								? (frame.floorPanel.floor.hero.getDEF() + tempStats[2])
										- getEnemies().get(getPos()).getDEX()
								: 0);
			}
			if (damage < 0)
				damage = 0;
			frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP() - damage);
			System.out.println("The enemy attacked you and you took " + damage + " damage.");
			damage = 0;
			
			if (frame.floorPanel.floor.hero.getPassive() != null) {
				switch (frame.floorPanel.floor.hero.getPassive()) {
				case Bloodlust:
					int n = r.nextInt(0, 7);
					tempStats[n] += 1;
					switch (n) {
					case 0:
						frame.combatPanel.hero.get(n + 1)
								.setText("STA: " + (frame.floorPanel.floor.hero.getSTA() + tempStats[n]));
						break;
					case 1:
						frame.combatPanel.hero.get(n + 1)
								.setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[n]));
						break;
					case 2:
						frame.combatPanel.hero.get(n + 1)
								.setText("DEX: " + (frame.floorPanel.floor.hero.getDEX() + tempStats[n]));
						break;
					case 3:
						frame.combatPanel.hero.get(n + 1)
								.setText("DEF: " + (frame.floorPanel.floor.hero.getDEF() + tempStats[n]));
						break;
					case 4:
						frame.combatPanel.hero.get(n + 1)
								.setText("INT: " + (frame.floorPanel.floor.hero.getINT() + tempStats[n]));
						break;
					case 5:
						frame.combatPanel.hero.get(n + 1)
								.setText("MAS: " + (frame.floorPanel.floor.hero.getMAS() + tempStats[n]));
						break;
					case 6:
						frame.combatPanel.hero.get(n + 1)
								.setText("CHA: " + (frame.floorPanel.floor.hero.getCHA() + tempStats[n]));
						break;
					}
					break;
				case Forgotten:
					if (r.nextInt(0, 100) < 20) {
						enemies.get(pos).setCurrentHP(
								(int) (enemies.get(pos).getCurrentHP() - frame.floorPanel.floor.hero.getDEX() * 2 / 5));
						try {
							frame.combatPanel.enemy.get(8).setBackground(Color.GREEN);
							frame.combatPanel.enemy.get(8).setVisible(true);
							Thread.sleep(100);
							frame.combatPanel.enemy.get(8).setVisible(false);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					break;
				case Brute:
					if (frame.floorPanel.floor.hero.getCurrentHP() >= frame.floorPanel.floor.hero.getMaxHP() / 2
							&& isUsed == false) {
						tempStats[1] += 3;
						isUsed = true;
					} else if (frame.floorPanel.floor.hero.getCurrentHP() < frame.floorPanel.floor.hero.getMaxHP() / 2
							&& isUsed == true) {
						tempStats[1] -= 3;
					}
					frame.combatPanel.hero.get(2).setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
					break;
				case Wise:
					int n1 = r.nextInt(0, 7);
					switch (n1) {
					case 0:
						enemies.get(pos).setSTA(enemies.get(pos).getSTA() - 1);
						if (enemies.get(pos).getSTA() < 0) {
							enemies.get(pos).setSTA(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("STA: " + enemies.get(pos).getSTA());
						break;
					case 1:
						enemies.get(pos).setSTR(enemies.get(pos).getSTR() - 1);
						if (enemies.get(pos).getSTR() < 0) {
							enemies.get(pos).setSTR(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("STR: " + enemies.get(pos).getSTR());
						break;
					case 2:
						enemies.get(pos).setDEX(enemies.get(pos).getDEX() - 1);
						if (enemies.get(pos).getDEX() < 0) {
							enemies.get(pos).setDEX(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("DEX: " + enemies.get(pos).getDEX());
						break;
					case 3:
						enemies.get(pos).setDEF(enemies.get(pos).getDEF() - 1);
						if (enemies.get(pos).getDEF() < 0) {
							enemies.get(pos).setDEF(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("DEF: " + enemies.get(pos).getDEF());
						break;
					case 4:
						enemies.get(pos).setINT(enemies.get(pos).getINT() - 1);
						if (enemies.get(pos).getINT() < 0) {
							enemies.get(pos).setINT(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("INT: " + enemies.get(pos).getINT());
						break;
					case 5:
						enemies.get(pos).setMAS(enemies.get(pos).getMAS() - 1);
						if (enemies.get(pos).getMAS() < 0) {
							enemies.get(pos).setMAS(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("MAS: " + enemies.get(pos).getMAS());
						break;
					case 6:
						enemies.get(pos).setCHA(enemies.get(pos).getCHA() - 1);
						if (enemies.get(pos).getCHA() < 0) {
							enemies.get(pos).setCHA(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("CHA: " + enemies.get(pos).getCHA());
						break;
					}
					break;
				case Ferocious:
					frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP() + 1);
					if (frame.floorPanel.floor.hero.getCurrentHP() > frame.floorPanel.floor.hero.getMaxHP()) {
						frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getMaxHP());
					}
					break;
				case Unyielding:
					if (frame.floorPanel.floor.hero.getCurrentHP() <= frame.floorPanel.floor.hero.getMaxHP() / 2
							&& isUsed == false) {
						frame.floorPanel.floor.hero.setCurrentHP((int) (frame.floorPanel.floor.hero.getMaxHP() * 7 / 10));
						tempStats[1] += 4;
						frame.combatPanel.hero.get(2)
								.setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
						isUsed = true;
						System.out.println("passive used");
					}
					break;
				case Lustful:
					if (enemies.get(pos).getCurrentHP() > 0) {
						tempStats[1] += (int) (enemies.get(pos).getMaxHP() / enemies.get(pos).getCurrentHP()) - 1;
						tempStats[4] += (int) (enemies.get(pos).getMaxHP() / enemies.get(pos).getCurrentHP()) - 1;
						tempStats[5] += (int) (enemies.get(pos).getMaxHP() / enemies.get(pos).getCurrentHP()) - 1;
						frame.combatPanel.hero.get(2)
								.setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
						frame.combatPanel.hero.get(5)
								.setText("INT: " + (frame.floorPanel.floor.hero.getINT() + tempStats[4]));
						frame.combatPanel.hero.get(6)
								.setText("MAS: " + (frame.floorPanel.floor.hero.getMAS() + tempStats[5]));
					}
					break;
				}
			}
			
			if (frame.floorPanel.floor.hero.getCurrentHP() <= 0) {
				frame.endPanel.gameEnd("-");
			}
		}
		frame.combatPanel.repaint();
	}// enemy combat

	public synchronized void bossFight() {
		damage = useSkill(skill, "boss")
				- ((getBoss().getDEF() - (frame.floorPanel.floor.hero.getDEX() + tempStats[2])) >= 0
						? getBoss().getDEF() - (frame.floorPanel.floor.hero.getDEX() + tempStats[2])
						: 0);
		if (damage < 0)
			damage = 0;
		getBoss().setCurrentHP(getBoss().getCurrentHP() - damage);
		System.out.println("You used " + skill + " and dealt " + damage + " damage.");

		if (getBoss().getCurrentHP() <= 0) {
			if (getBoss().getName().equals("Artemes")) {
				frame.endPanel.gameEnd("+");
				return;
			}

			for (int i = 0; i < tempStats.length; i++) {
				tempStats[i] = 0;
			}

			isUsed = false;
			frame.floorPanel.setVisible(true);
			frame.combatPanel.setVisible(false);
			frame.textPanel.clearButtons();
			frame.textPanel.infoSwitch.setVisible(true);
			frame.floorPanel.floor.hero
					.setXP(frame.floorPanel.floor.hero.getXP() + 50 * frame.floorPanel.floor.getFloor());
			frame.floorPanel.floor.hero.lvlUP();

			if (!frame.textPanel.heading.getText().equals("EVOLVE")) {
				frame.command.readCommand("info");
			}

			setBoss(null);
			frame.floorPanel.floor.hero.setPosX(0);
			frame.floorPanel.floor.hero.setPosY(0);
			frame.floorPanel.floor.nextFloor();
			if (frame.floorPanel.floor.getFloor() != 8) {
				frame.floor.setText("Floor " + frame.floorPanel.floor.getFloor());
			} else {
				frame.floor.setText("Floor ???");
			}
			frame.floorPanel.floor.changeImage();
		}

		else {
			int crit = r.nextInt(0, 100);
			if (crit <= getBoss().getMAS()) {

				try {
					frame.combatPanel.hero.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.hero.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((getBoss().getSTR() * (1.5 + 0.01 * getBoss().getCHA()))
						- (((frame.floorPanel.floor.hero.getDEF() + tempStats[3]) - getBoss().getDEX()) >= 0
								? (frame.floorPanel.floor.hero.getDEF() + tempStats[3]) - getBoss().getDEX()
								: 0));
			} else {
				damage = getBoss().getSTR()
						- (((frame.floorPanel.floor.hero.getDEF() + tempStats[3]) - getBoss().getDEX()) >= 0
								? (frame.floorPanel.floor.hero.getDEF() + tempStats[3]) - getBoss().getDEX()
								: 0);
			}

			if (damage < 0)
				damage = 0;
			frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP() - damage);
			System.out.println(getBoss().getName() + " attacked you and you took " + damage + " damage.");
			damage = 0;
			
			if (frame.floorPanel.floor.hero.getPassive() != null) {
				switch (frame.floorPanel.floor.hero.getPassive()) {
				case Bloodlust:
					int n = r.nextInt(0, 7);
					tempStats[n] += 1;
					switch (n) {
					case 0:
						frame.combatPanel.hero.get(n + 1)
								.setText("STA: " + (frame.floorPanel.floor.hero.getSTA() + tempStats[n]));
						break;
					case 1:
						frame.combatPanel.hero.get(n + 1)
								.setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[n]));
						break;
					case 2:
						frame.combatPanel.hero.get(n + 1)
								.setText("DEX: " + (frame.floorPanel.floor.hero.getDEX() + tempStats[n]));
						break;
					case 3:
						frame.combatPanel.hero.get(n + 1)
								.setText("DEF: " + (frame.floorPanel.floor.hero.getDEF() + tempStats[n]));
						break;
					case 4:
						frame.combatPanel.hero.get(n + 1)
								.setText("INT: " + (frame.floorPanel.floor.hero.getINT() + tempStats[n]));
						break;
					case 5:
						frame.combatPanel.hero.get(n + 1)
								.setText("MAS: " + (frame.floorPanel.floor.hero.getMAS() + tempStats[n]));
						break;
					case 6:
						frame.combatPanel.hero.get(n + 1)
								.setText("CHA: " + (frame.floorPanel.floor.hero.getCHA() + tempStats[n]));
						break;
					}
					break;
				case Forgotten:
					if (r.nextInt(0, 100) < 20) {
						boss.setCurrentHP((int) (boss.getCurrentHP() - frame.floorPanel.floor.hero.getDEX() * 2 / 5));
					
						try {
							frame.combatPanel.enemy.get(8).setBackground(Color.GREEN);
							frame.combatPanel.enemy.get(8).setVisible(true);
							Thread.sleep(100);
							frame.combatPanel.enemy.get(8).setVisible(false);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					break;
				case Brute:
					if (frame.floorPanel.floor.hero.getCurrentHP() >= frame.floorPanel.floor.hero.getMaxHP() / 2
							&& isUsed == false) {
						tempStats[1] += 3;
						isUsed = true;
					} else if (frame.floorPanel.floor.hero.getCurrentHP() < frame.floorPanel.floor.hero.getMaxHP() / 2
							&& isUsed == true) {
						tempStats[1] -= 3;
					}
					frame.combatPanel.hero.get(2).setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
					break;
				case Wise:
					int n1 = r.nextInt(0, 7);
					switch (n1) {
					case 0:
						boss.setSTA(boss.getSTA() - 1);
						if (boss.getSTA() < 0) {
							boss.setSTA(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("STA: " + boss.getSTA());
						break;
					case 1:
						boss.setSTR(boss.getSTR() - 1);
						if (boss.getSTR() < 0) {
							boss.setSTR(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("STR: " + boss.getSTR());
						break;
					case 2:
						boss.setDEX(boss.getDEX() - 1);
						if (boss.getDEX() < 0) {
							boss.setDEX(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("DEX: " + boss.getDEX());
						break;
					case 3:
						boss.setDEF(boss.getDEF() - 1);
						if (boss.getDEF() < 0) {
							boss.setDEF(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("DEF: " + boss.getDEF());
						break;
					case 4:
						boss.setINT(boss.getINT() - 1);
						if (boss.getINT() < 0) {
							boss.setINT(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("INT: " + boss.getINT());
						break;
					case 5:
						boss.setMAS(boss.getMAS() - 1);
						if (boss.getMAS() < 0) {
							boss.setMAS(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("MAS: " + boss.getMAS());
						break;
					case 6:
						boss.setCHA(boss.getCHA() - 1);
						if (boss.getCHA() < 0) {
							boss.setCHA(0);
						}
						frame.combatPanel.enemy.get(n1 + 1).setText("CHA: " + boss.getCHA());
						break;
					}
					break;
				case Ferocious:
					frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP() + 1);
					if (frame.floorPanel.floor.hero.getCurrentHP() > frame.floorPanel.floor.hero.getMaxHP()) {
						frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getMaxHP());
					}
					break;
				case Unyielding:
					if (frame.floorPanel.floor.hero.getCurrentHP() <= frame.floorPanel.floor.hero.getMaxHP() / 2
							&& isUsed == false) {
						frame.floorPanel.floor.hero.setCurrentHP((int) (frame.floorPanel.floor.hero.getMaxHP() * 7 / 10));
						tempStats[1] += 4;
						frame.combatPanel.hero.get(2)
								.setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
						isUsed = true;
						System.out.println("passive used");
					}
					break;
				case Lustful:
					if (boss.getCurrentHP() > 0) {
						tempStats[1] += (int) (boss.getMaxHP() / boss.getCurrentHP()) - 1;
						tempStats[4] += (int) (boss.getMaxHP() / boss.getCurrentHP()) - 1;
						tempStats[5] += (int) (boss.getMaxHP() / boss.getCurrentHP()) - 1;
						frame.combatPanel.hero.get(2)
								.setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
						frame.combatPanel.hero.get(5)
								.setText("INT: " + (frame.floorPanel.floor.hero.getINT() + tempStats[4]));
						frame.combatPanel.hero.get(6)
								.setText("MAS: " + (frame.floorPanel.floor.hero.getMAS() + tempStats[5]));
					}
					break;
				}
			}
			
			if (frame.floorPanel.floor.hero.getCurrentHP() <= 0) {
				frame.endPanel.gameEnd("-");
			}
		}
		frame.combatPanel.repaint();
	}// boss combat

	public int useSkill(String skill, String type) {
		int crit = r.nextInt(0, 100);
		switch (skill) {
		case "Hit": // deals 100% str dmg
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((frame.floorPanel.floor.hero.getSTR() + tempStats[1]) * 1
						* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
			} else
				damage = (int) (frame.floorPanel.floor.hero.getSTR() + tempStats[1]) * 1;
			break;

		case "Demonic Trance": // str+2 dex+2 def+1 cha+2
			tempStats[1] += 2;
			tempStats[2] += 2;
			tempStats[3] += 1;
			tempStats[6] += 2;

			frame.combatPanel.hero.get(2).setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
			frame.combatPanel.hero.get(3).setText("DEX: " + (frame.floorPanel.floor.hero.getDEX() + tempStats[2]));
			frame.combatPanel.hero.get(4).setText("DEF: " + (frame.floorPanel.floor.hero.getDEF() + tempStats[3]));
			frame.combatPanel.hero.get(7).setText("CHA: " + (frame.floorPanel.floor.hero.getCHA() + tempStats[6]));

			damage = 0;
			break;

		case "Blood Lad": // deals 50% currentHP damage and heals for that amount
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((frame.floorPanel.floor.hero.getCurrentHP() + tempStats[0]) * 0.5
						* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
			} else
				damage = (int) ((frame.floorPanel.floor.hero.getCurrentHP() + tempStats[0]) * 0.5);
			frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP() + damage);

			if (frame.floorPanel.floor.hero.getCurrentHP() > frame.floorPanel.floor.hero.getMaxHP())
				frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getMaxHP());
			break;

		case "Blood Dance": // deals 100% maxHP damage
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((frame.floorPanel.floor.hero.getMaxHP() + tempStats[0]) * 1
						* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
			} else
				damage = (int) ((frame.floorPanel.floor.hero.getCurrentHP() + tempStats[0]) * 1);
			break;

		case "Phase": // deals 100% str + 50% dex damage
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((frame.floorPanel.floor.hero.getSTR() + tempStats[1]
						+ (frame.floorPanel.floor.hero.getDEX() + tempStats[2]) * 0.5)
						* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
			} else
				damage = (int) ((frame.floorPanel.floor.hero.getSTR() + tempStats[1]
						+ (frame.floorPanel.floor.hero.getDEX() + tempStats[2]) * 0.5) * 1);
			break;

		case "Possess": // deals dex% of enemy maxHP
			if (type.equals("enemy")) {
				damage = (int) (getEnemies().get(getPos()).getMaxHP() * (frame.floorPanel.floor.hero.getDEX() / 100));
			} else {
				damage = (int) (getBoss().getMaxHP() * (frame.floorPanel.floor.hero.getDEX() / 100));
			}
			break;

		case "Smash": // deals 100% str + 20% def damage
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((frame.floorPanel.floor.hero.getSTR() + tempStats[1]
						+ (frame.floorPanel.floor.hero.getDEF() + tempStats[3]) * 0.2)
						* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
			} else
				damage = (int) (frame.floorPanel.floor.hero.getSTR() + tempStats[1]
						+ (frame.floorPanel.floor.hero.getDEF() + tempStats[3]) * 0.2);
			break;

		case "Rough": // def+5 str+2 mas+3 heals 3hp
			tempStats[3] += 5;
			tempStats[1] += 2;
			tempStats[5] += 3;
			frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP() + 3);
			if (frame.floorPanel.floor.hero.getCurrentHP() > frame.floorPanel.floor.hero.getMaxHP())
				frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getMaxHP());

			frame.combatPanel.hero.get(2).setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
			frame.combatPanel.hero.get(4).setText("DEF: " + (frame.floorPanel.floor.hero.getDEF() + tempStats[3]));
			frame.combatPanel.hero.get(6).setText("MAS: " + (frame.floorPanel.floor.hero.getMAS() + tempStats[5]));

			damage = 0;
			break;
		case "Arcane Blast": // deals 200% int damage
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((frame.floorPanel.floor.hero.getINT() + tempStats[4]) * 2
						* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
			} else
				damage = (int) ((frame.floorPanel.floor.hero.getINT() + tempStats[4]) * 2);
			break;
		case "Deviant": // int+10 def+2 mas+6
			tempStats[4] += 10;
			tempStats[3] += 2;
			tempStats[5] += 6;

			frame.combatPanel.hero.get(4).setText("DEF: " + (frame.floorPanel.floor.hero.getDEF() + tempStats[3]));
			frame.combatPanel.hero.get(5).setText("INT: " + (frame.floorPanel.floor.hero.getINT() + tempStats[4]));
			frame.combatPanel.hero.get(6).setText("MAS: " + (frame.floorPanel.floor.hero.getMAS() + tempStats[5]));

			damage = 0;
			break;
		case "Berserk": // str+4 dex+5 heals for 20%hp if under 50%
			if (tempStats[1] == 0 || tempStats[2] == 0) {
				tempStats[1] += 4;
				tempStats[2] += 5;
			}
			if (frame.floorPanel.floor.hero.getCurrentHP() <= (frame.floorPanel.floor.hero.getMaxHP() * 0.5)) {
				frame.floorPanel.floor.hero.setCurrentHP((int) (frame.floorPanel.floor.hero.getCurrentHP()
						+ frame.floorPanel.floor.hero.getMaxHP() * 0.2));
				if (frame.floorPanel.floor.hero.getCurrentHP() > frame.floorPanel.floor.hero.getMaxHP())
					frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP());
			}

			frame.combatPanel.hero.get(2).setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
			frame.combatPanel.hero.get(3).setText("DEX: " + (frame.floorPanel.floor.hero.getDEX() + tempStats[2]));

			damage = 0;
			break;
		case "Devour": // deals enemyHP% str damage
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (type.equals("enemy")) {
					damage = (int) ((frame.floorPanel.floor.hero.getSTR() + tempStats[1])
							* (2 - getEnemies().get(getPos()).getCurrentHP() / 100)
							* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
				} else {
					damage = (int) ((frame.floorPanel.floor.hero.getSTR() + tempStats[1])
							* (2 - getBoss().getCurrentHP() / 100)
							* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
				}
			} else if (type.equals("enemy")) {
				damage = (int) (frame.floorPanel.floor.hero.getSTR() + tempStats[1])
						* (2 - getEnemies().get(getPos()).getCurrentHP() / 100);
			} else {
				damage = (int) (frame.floorPanel.floor.hero.getSTR() + tempStats[1])
						* (2 - getBoss().getCurrentHP() / 100);
			}
			break;
		case "True Slash": // deals 100% str damage (if it crits deals 100% maxHP)
			if (crit <= ((frame.floorPanel.floor.hero.getMAS() + tempStats[5]) / 2)) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (type.equals("enemy")) {
					damage = (int) (getEnemies().get(getPos()).getMaxHP());
				} else {
					damage = (int) (getBoss().getMaxHP());
				}
			} else
				damage = (int) (frame.floorPanel.floor.hero.getSTR() + tempStats[1]) * 1;
			break;
		case "Dead Calm": // mas+=15 str+=2 def+=3
			tempStats[5] += 15;
			tempStats[1] += 2;
			tempStats[3] += 3;
			frame.combatPanel.hero.get(2).setText("STR: " + (frame.floorPanel.floor.hero.getSTR() + tempStats[1]));
			frame.combatPanel.hero.get(4).setText("DEF: " + (frame.floorPanel.floor.hero.getDEF() + tempStats[3]));
			frame.combatPanel.hero.get(6).setText("MAS: " + (frame.floorPanel.floor.hero.getMAS() + tempStats[6]));
			damage = 0;
			break;
		case "Charm": // deals 100% int and 20% cha damage, if crit - heals for 50% cha dmg
			if (crit <= (frame.floorPanel.floor.hero.getMAS() + tempStats[5])) {
				System.out.println("crit");

				try {
					frame.combatPanel.enemy.get(8).setBackground(Color.YELLOW);
					frame.combatPanel.enemy.get(8).setVisible(true);
					Thread.sleep(100);
					frame.combatPanel.enemy.get(8).setVisible(false);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				damage = (int) ((frame.floorPanel.floor.hero.getINT() + tempStats[4]
						+ (frame.floorPanel.floor.hero.getCHA() + tempStats[6]) * 0.2)
						* ((150 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
				frame.floorPanel.floor.hero.setCurrentHP((int) (frame.floorPanel.floor.hero.getCurrentHP()
						+ frame.floorPanel.floor.hero.getCHA() * 0.5));
				if (frame.floorPanel.floor.hero.getCurrentHP() > frame.floorPanel.floor.hero.getMaxHP())
					frame.floorPanel.floor.hero.setCurrentHP(frame.floorPanel.floor.hero.getCurrentHP());
			} else
				damage = (int) ((frame.floorPanel.floor.hero.getINT() + tempStats[4]
						+ (frame.floorPanel.floor.hero.getCHA() + tempStats[6]) * 0.2));
			break;
		case "Man Eater": // deals a guaranteed crit of 120% int damage(base crit damage 200%)
			damage = (int) ((frame.floorPanel.floor.hero.getINT() + tempStats[4] * 1.2)
					* ((200 + frame.floorPanel.floor.hero.getCHA() + tempStats[6]) / 100));
			break;
		}

		for (Button button : frame.textPanel.getButton()) {
			if (!button.getLabel().equals(skill) && button.isEnabled() == false
					&& !(button.getLabel().equals("Demonic Trance") || button.getLabel().equals("Rough")
							|| button.getLabel().equals("Deviant") || button.getLabel().equals("Dead Calm"))) {
				button.setEnabled(true);
				button.setBackground(new Color(30, 30, 30));
			}
		}

		return damage;
	}// skill use

	public void enemyCheck(BufferedReader br) {
		setBoss(frame.floorPanel.floor.boss);
		setEnemies(frame.floorPanel.floor.enemies);
		this.sc = br;
		if (getBoss().getPosX() == frame.floorPanel.floor.hero.getPosX()
				&& getBoss().getPosY() == frame.floorPanel.floor.hero.getPosY()) {
			frame.skillPanel.setVisible(false);
			frame.textPanel.setVisible(true);
			frame.textPanel.combatButtons();
			frame.floorPanel.setVisible(false);
			frame.combatPanel.setVisible(true);
			frame.textPanel.setEnemyType("boss");

			frame.combatPanel.enemy.get(0).setText(getBoss().getName());
			frame.combatPanel.enemy.get(1).setText("STA: " + boss.getSTA());
			frame.combatPanel.enemy.get(2).setText("STR: " + boss.getSTR());
			frame.combatPanel.enemy.get(3).setText("DEX: " + boss.getDEX());
			frame.combatPanel.enemy.get(4).setText("DEF: " + boss.getDEF());
			frame.combatPanel.enemy.get(5).setText("INT: " + boss.getINT());
			frame.combatPanel.enemy.get(6).setText("MAS: " + boss.getMAS());
			frame.combatPanel.enemy.get(7).setText("CHA: " + boss.getCHA());

			frame.combatPanel.hero.get(0).setText(frame.floorPanel.floor.hero.getName());
			frame.combatPanel.hero.get(1).setText("STA: " + frame.floorPanel.floor.hero.getSTA());
			frame.combatPanel.hero.get(2).setText("STR: " + frame.floorPanel.floor.hero.getSTR());
			frame.combatPanel.hero.get(3).setText("DEX: " + frame.floorPanel.floor.hero.getDEX());
			frame.combatPanel.hero.get(4).setText("DEF: " + frame.floorPanel.floor.hero.getDEF());
			frame.combatPanel.hero.get(5).setText("INT: " + frame.floorPanel.floor.hero.getINT());
			frame.combatPanel.hero.get(6).setText("MAS: " + frame.floorPanel.floor.hero.getMAS());
			frame.combatPanel.hero.get(7).setText("CHA: " + frame.floorPanel.floor.hero.getCHA());
			frame.textPanel.infoSwitch.setVisible(false);
		}
		for (int p = 0; p < getEnemies().size(); p++) {
			if (getEnemies().get(p).getPosX() == frame.floorPanel.floor.hero.getPosX()
					&& getEnemies().get(p).getPosY() == frame.floorPanel.floor.hero.getPosY()) {
				frame.skillPanel.setVisible(false);
				frame.textPanel.setVisible(true);
				frame.textPanel.combatButtons();
				frame.floorPanel.setVisible(false);
				frame.combatPanel.setVisible(true);
				frame.textPanel.setEnemyType("enemy");

				frame.combatPanel.enemy.get(0).setText("Enemy");
				frame.combatPanel.enemy.get(1).setText("STA: " + enemies.get(p).getSTA());
				frame.combatPanel.enemy.get(2).setText("STR: " + enemies.get(p).getSTR());
				frame.combatPanel.enemy.get(3).setText("DEX: " + enemies.get(p).getDEX());
				frame.combatPanel.enemy.get(4).setText("DEF: " + enemies.get(p).getDEF());
				frame.combatPanel.enemy.get(5).setText("INT: " + enemies.get(p).getINT());
				frame.combatPanel.enemy.get(6).setText("MAS: " + enemies.get(p).getMAS());
				frame.combatPanel.enemy.get(7).setText("CHA: " + enemies.get(p).getCHA());

				frame.combatPanel.hero.get(0).setText(frame.floorPanel.floor.hero.getName());
				frame.combatPanel.hero.get(1).setText("STA: " + frame.floorPanel.floor.hero.getSTA());
				frame.combatPanel.hero.get(2).setText("STR: " + frame.floorPanel.floor.hero.getSTR());
				frame.combatPanel.hero.get(3).setText("DEX: " + frame.floorPanel.floor.hero.getDEX());
				frame.combatPanel.hero.get(4).setText("DEF: " + frame.floorPanel.floor.hero.getDEF());
				frame.combatPanel.hero.get(5).setText("INT: " + frame.floorPanel.floor.hero.getINT());
				frame.combatPanel.hero.get(6).setText("MAS: " + frame.floorPanel.floor.hero.getMAS());
				frame.combatPanel.hero.get(7).setText("CHA: " + frame.floorPanel.floor.hero.getCHA());
				this.setPos(p);
				frame.textPanel.infoSwitch.setVisible(false);

				break;
			}
		}
		frame.combatPanel.repaint();
	}// check for fight

	public int getDamage() {
		return damage;
	}

	public void setDamage(int d) {
		damage = d;
	}// damage

	public String getSkill() {
		return skill;
	}

	public void setSkill(String s) {
		skill = s;
	}// damage

	public int[] getTempStats() {
		return tempStats;
	}

	public void setTempStats(int[] s) {
		tempStats = s;
	}// temp stats

	public int getPos() {
		return pos;
	}

	public void setPos(int p) {
		this.pos = p;
	}

	public List<Enemy> getEnemies() {
		return enemies;
	}

	public void setEnemies(List<Enemy> enemies) {
		this.enemies = enemies;
	}

	public Boss getBoss() {
		return boss;
	}

	public void setBoss(Boss boss) {
		this.boss = boss;
	}
}
