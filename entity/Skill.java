package entity;

public enum Skill {
	Hit, Demonic_Trance,
	Blood_Lad, Blood_Dance,
	Phase, Possess,
	Smash, Rough,
	Arcane_Blast, Deviant,
	Berserk, Devour,
	True_Slash, Dead_Calm,
	Charm, Man_Eater;
}
