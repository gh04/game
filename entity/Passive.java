package entity;

public enum Passive {
	Bloodlust,
	Forgotten,
	Brute,
	Wise,
	Ferocious,
	Unyielding,
	Lustful;
}
