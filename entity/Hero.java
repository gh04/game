package entity;

import game.*;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import javax.imageio.ImageIO;

import game.Floor;

public class Hero extends Entity implements Serializable {
	private static final long serialVersionUID = 1L;
	public transient BufferedReader sc;
	// info
	private String name;
	private String race = "Cambion";
	private String gender;
	// description
	private Color eyes;
	private boolean hasAdmin = true;
	// leveling
	private int level = 1;
	private int xp = 0;
	private int statPoints = 0;
	// skills
	private List<String> skills = new LinkedList<String>();
	private Passive passive = null;
	public transient Command cmd;

	public Hero(int x, int y) {
		this.setPosX(x);
		this.setPosY(y);
		skills.add(Skill.Hit.toString().replaceAll("_", " "));
		this.setMaxHP(6 + this.getSTA());
		this.setCurrentHP(this.getMaxHP());

		this.changeImage();

	}// constructor for hero

	public void move(String c, Floor m) {
		switch (c) {
		case "w":
			if (this.getPosY() != 0) {
				this.setPosY(this.getPosY() - 1);
			}
			break;
		case "s":
			if (this.getPosY() != m.getSizeY() - 1) {
				this.setPosY(this.getPosY() + 1);
			}
			break;
		case "a":
			if (this.getPosX() != 0) {
				this.setPosX(this.getPosX() - 1);
			}
			break;
		case "d":
			if (this.getPosX() != m.getSizeX() - 1) {
				this.setPosX(this.getPosX() + 1);
			}
			break;
		}
	}// movement

	public void lvlUP() {
		while (xp >= 10 * level) {
			xp -= 10 * level;
			level += 1;
			statPoints++;
			this.setCurrentHP(this.getMaxHP());
			System.out.println("You leveled up!");
			System.out.println("You gained 1 stat point");
			System.out.println("Your health has recovered");
			if (level == 5 || level == 10 || level == 20 || level == 30) {
				this.evolve();
			}
		}
	}// level up

	public void pointUse() {
		String in;
		System.out.println(
				"Write a stat abbreviation to put one skill point in it.\nIf you want to save your points use 'exit' to leave.");
		System.out.println("STA: " + this.getSTA());
		System.out.println("STR: " + this.getSTR());
		System.out.println("DEX: " + this.getDEX());
		System.out.println("DEF: " + this.getDEF());
		System.out.println("INT: " + this.getINT());
		System.out.println("MAT: " + this.getMAS());
		System.out.println("CHA: " + this.getCHA());
		while (true) {
			System.out.println("You have " + statPoints + " stat points remaining.");
			if (statPoints <= 0)
				break;
			try {
				in = sc.readLine().toLowerCase();
			} catch (IOException e) {
				in = "";
			}
			if (in.equals("exit")) {
				System.out.println("You have saved " + statPoints + " stat points.");
				break;
			}
			switch (in) {
			case "sta":
				this.setSTA(this.getSTA() + 1);
				statPoints--;
				this.setMaxHP(6 + this.getSTA());
				this.setCurrentHP(this.getCurrentHP() + 1);
				System.out.println("Stamina increased");
				break;
			case "str":
				this.setSTR(this.getSTR() + 1);
				statPoints--;
				System.out.println("Strength increased");
				break;
			case "dex":
				this.setDEX(this.getDEX() + 1);
				statPoints--;
				System.out.println("Dexterity increased");
				break;
			case "def":
				this.setDEF(this.getDEF() + 1);
				statPoints--;
				System.out.println("Defence increased");
				break;
			case "int":
				this.setINT(this.getINT() + 1);
				statPoints--;
				System.out.println("Intelligence increased");
				break;
			case "mas":
				this.setMAS(this.getMAS() + 1);
				statPoints--;
				System.out.println("Mastery increased");
				break;
			case "cha":
				this.setCHA(this.getCHA() + 1);
				statPoints--;
				System.out.println("Charisma increased");
				break;
			default:
				System.out.println("Invalid input. Use one of the given options.");
				break;
			}
		}
	}// stat point use

	public void evolve() {
		if (level == 5) {
			race = "Lesser Demon";
			this.setSTA(this.getSTA() + 2);
			this.setSTR(this.getSTR() + 1);
			this.setDEX(this.getDEX() + 0);
			this.setDEF(this.getDEF() + 1);
			this.setINT(this.getINT() + 1);
			this.setMAS(this.getMAS() + 0);
			this.setCHA(this.getCHA() + 2);
		} else if (level == 10) {
			race = "Demon";
			this.addSkill(Skill.Demonic_Trance.toString().replaceAll("_", " "));
			this.setSTA(this.getSTA() + 2);
			this.setSTR(this.getSTR() + 1);
			this.setDEX(this.getDEX() + 1);
			this.setDEF(this.getDEF() + 1);
			this.setINT(this.getINT() + 2);
			this.setMAS(this.getMAS() + 1);
			this.setCHA(this.getCHA() + 2);
		} else if (level == 20) {
			cmd.frame.textPanel.clearAll();
			cmd.frame.textPanel.infoSwitch.setVisible(false);
			cmd.frame.textPanel.evolve();
			cmd.frame.textPanel.repaint();
		} else if (level == 30) {
			switch (race) {
			case "Dhampir":
				this.setRace("Vampire"); // sta
				this.addSkill(Skill.Blood_Dance.toString().replaceAll("_", " "));
				this.setSTA(this.getSTA() + 12);
				this.setSTR(this.getSTR() + 2);
				this.setDEX(this.getDEX() + 4);
				this.setDEF(this.getDEF() + 3);
				this.setINT(this.getINT() + 7);
				this.setMAS(this.getMAS() + 4);
				this.setCHA(this.getCHA() + 8);
				break;
			case "Ghost":
				this.setRace("Wraith"); // dex
				this.addSkill(Skill.Possess.toString().replaceAll("_", " "));
				this.setSTA(this.getSTA() + 5);
				this.setSTR(this.getSTR() + 7);
				this.setDEX(this.getDEX() + 9);
				this.setDEF(this.getDEF() + 4);
				this.setINT(this.getINT() + 2);
				this.setMAS(this.getMAS() + 8);
				this.setCHA(this.getCHA() + 5);
				break;
			case "Ogre":
				this.setRace("Minotaur"); // def
				this.addSkill(Skill.Rough.toString().replaceAll("_", " "));
				this.setSTA(this.getSTA() + 10);
				this.setSTR(this.getSTR() + 10);
				this.setDEX(this.getDEX() + 0);
				this.setDEF(this.getDEF() + 11);
				this.setINT(this.getINT() + 0);
				this.setMAS(this.getMAS() + 6);
				this.setCHA(this.getCHA() + 3);
				break;
			case "Ghoul":
				this.setRace("Lich"); // int
				this.addSkill(Skill.Deviant.toString().replaceAll("_", " "));
				this.setSTA(this.getSTA() + 3);
				this.setSTR(this.getSTR() + 3);
				this.setDEX(this.getDEX() + 3);
				this.setDEF(this.getDEF() + 4);
				this.setINT(this.getINT() + 16);
				this.setMAS(this.getMAS() + 8);
				this.setCHA(this.getCHA() + 3);
				break;
			case "Beast":
				this.setRace("Himera"); // str
				this.addSkill(Skill.Devour.toString().replaceAll("_", " "));
				this.setSTA(this.getSTA() + 2);
				this.setSTR(this.getSTR() + 10);
				this.setDEX(this.getDEX() + 5);
				this.setDEF(this.getDEF() + 5);
				this.setINT(this.getINT() + 0);
				this.setMAS(this.getMAS() + 9);
				this.setCHA(this.getCHA() + 9);
				break;
			case "Majin":
				this.setRace("Oni"); // mas
				this.addSkill(Skill.Dead_Calm.toString().replaceAll("_", " "));
				this.setSTA(this.getSTA() + 7);
				this.setSTR(this.getSTR() + 6);
				this.setDEX(this.getDEX() + 2);
				this.setDEF(this.getDEF() + 2);
				this.setINT(this.getINT() + 0);
				this.setMAS(this.getMAS() + 14);
				this.setCHA(this.getCHA() + 9);
				break;
			case "Imp":
				this.setRace("Succubus"); // cha
				this.addSkill(Skill.Man_Eater.toString().replaceAll("_", " "));
				this.setSTA(this.getSTA() + 3);
				this.setSTR(this.getSTR() + 0);
				this.setDEX(this.getDEX() + 2);
				this.setDEF(this.getDEF() + 2);
				this.setINT(this.getINT() + 8);
				this.setMAS(this.getMAS() + 7);
				this.setCHA(this.getCHA() + 18);
				break;
			}
		}
		this.setMaxHP(6 + this.getSTA());
		this.setCurrentHP(this.getMaxHP());
		this.changeImage();
	}// evolution

	public void changeImage() {
		try {
			this.setImg(ImageIO
					.read(new File("art/minis/hero/" + this.getRace().toLowerCase().replaceAll(" ", "_") + ".png")));
		} catch (IOException e) {
			this.setImg(null);
		}
	}

	public void printSkills() {
		System.out.println("~~~Skills~~~");
		for (int i = 0; i < skills.toArray().length; i++) {
			System.out.println((i + 1) + "." + skills.get(i));
		}
	}

	public List<String> getSkills() {
		return skills;
	}

	public void addSkill(String s) {
		skills.add(s.replaceAll("_", " "));
	}// skills

	public Passive getPassive() {
		return passive;
	}

	public void setPassive(Passive p) {
		passive = p;
	}// passive

	public boolean isAdmin() {
		return hasAdmin;
	}

	public void setAdmin(boolean b) {
		hasAdmin = b;
	}// admin

	public int getLevel() {
		return level;
	}

	public void setLevel(int l) {
		level = l;
	}// level

	public int getXP() {
		return xp;
	}

	public void setXP(int x) {
		xp = x;
	}// xp

	public int getSP() {
		return statPoints;
	}

	public void setSP(int s) {
		statPoints = s;
	}// stat points

	public String getGender() {
		return gender;
	}

	public void setGender(String g) {
		while (true) {
			if (g.equals("male") || g.equals("female")) {
				switch (g) {
				case "male":
					gender = "Male";
					break;
				case "female":
					gender = "Female";
					break;
				}
				break;
			} else {
				System.out.println("Invalid input. Gender must be from default genders.");
				try {
					g = sc.readLine().toLowerCase();
				} catch (IOException e) {
					g = "";
				}
			}
		}
	}// gender

	public String getRace() {
		return race;
	}

	public void setRace(String r) {
		race = r;
	}// race

	public String getName() {
		return name;
	}

	public void setName(String n) {
		while (true) {
			if (n.length() < 15) {
				name = n;
				break;
			} else {
				System.out.println("Invalid input. Name must be under 15 characters.");
				try {
					n = sc.readLine();
				} catch (IOException e) {
					n = "";
				}
			}
		}
	}// name

	public Color getEyeColor() {
		return eyes;
	}

	public void setEyeColor(Color e) {
		eyes = e;
	}// eye color

	public void printInfo() {

		System.out.println("~~~~Info~~~~");
		System.out.println("Name: " + name);
		System.out.println("Gender: " + gender);
		System.out.println("Race: " + race);
		System.out.println("Level: " + level);
		System.out.println("XP: ");
		for (int i = 1; i <= 10; i++) {
			if (10 * level * i / 10 <= xp) {
				System.out.print("+ ");
			} else {
				System.out.print("- ");
			}
		}
		System.out.println();
	}// printing hero information

	public void printStats() {
		System.out.println("~~~~Stats~~~~");
		System.out.println("Health: " + this.getCurrentHP() + "/" + this.getMaxHP());
		System.out.println("STA: " + this.getSTA());
		System.out.println("STR: " + this.getSTR());
		System.out.println("DEX: " + this.getDEX());
		System.out.println("DEF: " + this.getDEF());
		System.out.println("INT: " + this.getINT());
		System.out.println("MAS: " + this.getMAS());
		System.out.println("CHA: " + this.getCHA());
		System.out.println("Stat Points: " + statPoints);
	}// printing hero stats
}
