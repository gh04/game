package entity;


import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

public class Boss extends Enemy implements Serializable {
	// boss differences
	private String name;

	public Boss(int x, int y, String n) {
		super(x, y);
		
		name = n;

		switch (name) {
		case "Belphegor":
			this.setSTA(7);
			this.setSTR(1);
			this.setDEX(3);
			this.setDEF(2);
			this.setINT(10);
			this.setMAS(5);
			this.setCHA(9);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		case "Mammon":
			this.setSTA(11);
			this.setSTR(2);
			this.setDEX(5);
			this.setDEF(3);
			this.setINT(1);
			this.setMAS(15);
			this.setCHA(10);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		case "Asmodeus":
			this.setSTA(15);
			this.setSTR(4);
			this.setDEX(7);
			this.setDEF(4);
			this.setINT(19);
			this.setMAS(10);
			this.setCHA(30);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		case "Leviathan":
			this.setSTA(21);
			this.setSTR(5);
			this.setDEX(23);
			this.setDEF(1);
			this.setINT(2);
			this.setMAS(14);
			this.setCHA(23);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		case "Beelzebub":
			this.setSTA(29);
			this.setSTR(2);
			this.setDEX(1);
			this.setDEF(20);
			this.setINT(3);
			this.setMAS(21);
			this.setCHA(43);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		case "Satan":
			this.setSTA(35);
			this.setSTR(10);
			this.setDEX(4);
			this.setDEF(3);
			this.setINT(1);
			this.setMAS(38);
			this.setCHA(62);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		case "Lucifer":
			this.setSTA(65);
			this.setSTR(12);
			this.setDEX(6);
			this.setDEF(5);
			this.setINT(34);
			this.setMAS(54);
			this.setCHA(93);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		case "Artemes":
			this.setSTA(69);
			this.setSTR(22);
			this.setDEX(1);
			this.setDEF(1);
			this.setINT(20);
			this.setMAS(5);
			this.setCHA(420);
			this.setMaxHP(5 + this.getSTA());
			this.setCurrentHP(this.getMaxHP());
			break;
		}
		this.changeImage();
	}
	
	public void changeImage() {
		try {
			this.setImg(ImageIO.read(new File("art/minis/boss/"+this.getName().toLowerCase().replaceAll(" ", "_")+".png")));
		} catch (IOException e) {
			this.setImg(null);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		name = n;
	}// name
}
