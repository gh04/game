package frame;

import listeners.*;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;

public class GameTextPanel extends Panel {
	public GameFrame frame;
	public Label heading;
	private List<Label> label = new LinkedList<Label>();
	private List<Button> button = new LinkedList<Button>();
	public Button infoSwitch;
	Font Normal = new Font("Normal", Font.PLAIN, 18);
	Font Bold = new Font("Bold", Font.BOLD, 20);
	BufferedImage img;
	File border = new File("art/textBorder.png");
	private String enemyType;

	GameTextPanel(GameFrame f) {
		frame = f;
		this.setBounds(490, 50, 301, 421);
		this.setBackground(new Color(40, 40, 40));

		heading = new Label();
		heading.setBounds(105, 20, 90, 30);
		heading.setForeground(new Color(255, 255, 255));
		heading.setFont(Bold);
		heading.setAlignment(Label.CENTER);
		this.add(heading);

		infoSwitch = new Button("STATS");
		infoSwitch.setBounds(105, 370, 100, 30);
		infoSwitch.setBackground(new Color(30, 30, 30));
		infoSwitch.setForeground(new Color(255, 255, 255));
		infoSwitch.setFont(Normal);
		InfoSwitchButtonListener isbl = new InfoSwitchButtonListener(this);
		infoSwitch.addActionListener(isbl);
		this.add(infoSwitch);

		this.setFocusable(false);
		this.setLayout(null);
		this.setVisible(false);
	}

	public void setHeading(String head) {
		heading.setText(head);
	}// heading

	public void setText(String text, int pos) {
		getLabel().add(new Label(text));
		getLabel().get(pos).setBounds(50, 80 + 30 * pos, 200, 30);
		getLabel().get(pos).setForeground(new Color(255, 255, 255));
		getLabel().get(pos).setFont(Normal);
		getLabel().get(pos).setAlignment(Label.LEFT);
		this.add(getLabel().get(pos));
	}// text

	public void clearAll() {
		clearLabels();
		clearButtons();
	}// clear all

	public void clearLabels() {
		for (int i = 0; i < getLabel().size(); i++) {
			this.remove(getLabel().get(i));
		}
		getLabel().clear();
	}// clear text

	public void clearButtons() {
		for (int i = 0; i < button.size(); i++) {
			this.remove(button.get(i));
		}
		button.clear();
	}// clear buttons

	public void combatButtons() {
		heading.setText("COMBAT");
		this.clearAll();
		for (int i = 0; i < frame.floorPanel.floor.hero.getSkills().size(); i++) {
			button.add(new Button(frame.floorPanel.floor.hero.getSkills().get(i)));
			button.get(i).setBounds(80, 80 + 40 * i, 140, 30);
			button.get(i).setBackground(new Color(30, 30, 30));
			button.get(i).setForeground(new Color(255, 255, 255));
			button.get(i).setFont(Normal);
			CombatButtonListener cbl = new CombatButtonListener(this, button.get(i));
			button.get(i).addActionListener(cbl);
			this.add(button.get(i));
		}
	}

	public void statPointButtons() {
		for (int i = 0; i < 7; i++) {
			button.add(new Button("+"));
			button.get(i).setBounds(200, 85 + 30 * i, 20, 20);
			button.get(i).setBackground(new Color(30, 30, 30));
			button.get(i).setForeground(new Color(255, 255, 255));
			button.get(i).setFont(Bold);
			StatPointButtonListener spbl = new StatPointButtonListener(this, button.get(i));
			button.get(i).addActionListener(spbl);
			this.add(button.get(i));
		}
	}

	public void skillButtons() {
		for (int i = 0; i < frame.floorPanel.floor.hero.getSkills().size(); i++) {
			button.add(new Button(frame.floorPanel.floor.hero.getSkills().get(i)));
			button.get(i).setBounds(80, 80 + 40 * i, 140, 30);
			button.get(i).setBackground(new Color(30, 30, 30));
			button.get(i).setForeground(new Color(255, 255, 255));
			button.get(i).setFont(Normal);
			SkillInfoButtonListener sibl = new SkillInfoButtonListener(this, button.get(i));
			button.get(i).addActionListener(sibl);
			this.add(button.get(i));
		}
		if (!(frame.floorPanel.floor.hero.getPassive()==null)) {
			button.add(new Button("Passive"));
			button.get(button.size() - 1).setBounds(105, 320, 100, 30);
			button.get(button.size() - 1).setBackground(new Color(30, 30, 30));
			button.get(button.size() - 1).setForeground(new Color(255, 255, 255));
			button.get(button.size() - 1).setFont(Normal);
			SkillInfoButtonListener sibl = new SkillInfoButtonListener(this, button.get(button.size() - 1));
			button.get(button.size() - 1).addActionListener(sibl);
			this.add(button.get(button.size() - 1));
		}
	}

	public void evolve() {
		heading.setText("EVOLVE");
		this.clearAll();
		button.add(new Button("Dhampir"));
		button.add(new Button("Ghost"));
		button.add(new Button("Ogre"));
		button.add(new Button("Ghoul"));
		button.add(new Button("Beast"));
		button.add(new Button("Majin"));
		button.add(new Button("Imp"));
		for (int i = 0; i < 7; i++) {
			button.get(i).setBounds(100, 80 + 40 * i, 100, 30);
			button.get(i).setBackground(new Color(30, 30, 30));
			button.get(i).setForeground(new Color(255, 255, 255));
			button.get(i).setFont(Normal);
			EvolveButtonListener ebl = new EvolveButtonListener(this, button.get(i));
			button.get(i).addActionListener(ebl);
			this.add(button.get(i));
		}
	}

	public void paint(Graphics g) {
		try {
			img = ImageIO.read(border);
		} catch (IOException e) {
			img = null;
		}
		g.drawImage(img, 0, 0, null);
		g.setColor(new Color(255, 255, 255));
		g.drawRect(0, 0, 300 - 1, 420 - 1);
		if (heading.getText().equals("INFO")) {
			g.fillRoundRect(51, 265,
					frame.floorPanel.floor.hero.getXP() / frame.floorPanel.floor.hero.getLevel() * 20 - 2, 20, 10, 10);
			g.drawRoundRect(51, 265, 198, 20, 10, 10);
		}
	}// background

	public String getEnemyType() {
		return enemyType;
	}

	public void setEnemyType(String e) {
		enemyType = e;
	}

	public List<Label> getLabel() {
		return label;
	}

	public void setLabel(List<Label> l) {
		label = l;
	}

	public List<Button> getButton() {
		return button;
	}

	public void setButton(List<Button> b) {
		button = b;
	}
}
