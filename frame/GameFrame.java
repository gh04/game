package frame;

import listeners.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import game.Command;
import game.Floor;

public class GameFrame extends Frame {
	
	public Command command;
	public GameFloorPanel floorPanel;
	public GameTextPanel textPanel;
	public GameHelpPanel helpPanel;
	public GameStartPanel startPanel;
	public GameCombatPanel combatPanel;
	public GameEndPanel endPanel;
	public GameSkillPanel skillPanel;
	public GameCreditsPanel creditsPanel;
	BufferedImage ico;
	public Button help;
	public Label floor;

	public GameFrame(Floor fl) {
		this.setSize(820, 500);
		this.setTitle("Engulfing Demonism");
		this.setBackground(new Color(45, 45, 45));
		this.setResizable(false);
		try {
			ico = ImageIO.read(new File("art/icon.png"));
		} catch (IOException e) {
			ico = null;
		}
		this.setIconImage(ico);

		GameWindowListener gwl = new GameWindowListener(this);
		this.addWindowListener(gwl);

		startPanel = new GameStartPanel(this);
		this.add(startPanel);

		floorPanel = new GameFloorPanel(fl);
		GameKeyListener gkl = new GameKeyListener(this);
		floorPanel.addKeyListener(gkl);
		this.add(floorPanel);

		textPanel = new GameTextPanel(this);
		this.add(textPanel);

		helpPanel = new GameHelpPanel(this);
		this.add(helpPanel);

		combatPanel = new GameCombatPanel(this);
		this.add(combatPanel);
		

		endPanel = new GameEndPanel(this);
		this.add(endPanel);
		
		skillPanel = new GameSkillPanel(this);
		this.add(skillPanel);
		
		creditsPanel = new GameCreditsPanel(this);
		this.add(creditsPanel);

		floor = new Label("Floor 1");
		floor.setBounds(35, 25, 100, 30);
		floor.setForeground(new Color(255, 255, 255));
		floor.setFont(new Font("Normal", Font.ITALIC, 15));
		this.add(floor);

		help = new Button("?");
		help.setBounds(795, 35, 15, 15);
		help.setBackground(new Color(30, 30, 30));
		help.setForeground(new Color(255, 255, 255));
		help.setFont(new Font("Normal", Font.PLAIN, 12));
		HelpButtonListener hbl = new HelpButtonListener(this);
		help.addActionListener(hbl);
		this.add(help);

		this.setLayout(null);
		this.setVisible(true);
	}

}
