package frame;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import listeners.CloseButtonListener;

public class GameEndPanel extends Panel {
	public GameFrame frame;
	Label heading;
	Font font = new Font("Bold", Font.BOLD, 30);
	Button opt1;
	Button opt2;
	File heroPfp;
	BufferedImage img;

	GameEndPanel(GameFrame f) {
		frame = f;
		this.setBounds(30, 50, 761, 421);
		this.setBackground(new Color(40, 40, 40));

		heading = new Label();
		heading.setBounds(80, 100, 600, 30);
		heading.setFont(font);
		heading.setForeground(new Color(255, 255, 255));
		heading.setAlignment(Label.CENTER);
		this.add(heading);

		opt1 = new Button("YES");
		opt1.setBounds(180, 200, 100, 30);
		opt1.setForeground(new Color(255, 255, 255));
		opt1.setBackground(new Color(30, 30, 30));
		opt1.setFont(new Font("Normal", Font.PLAIN, 18));
		CloseButtonListener yes = new CloseButtonListener(this, opt1);
		opt1.addActionListener(yes);
		opt1.setVisible(false);
		this.add(opt1);

		opt2 = new Button("NO");
		opt2.setBounds(490, 200, 100, 30);
		opt2.setForeground(new Color(255, 255, 255));
		opt2.setBackground(new Color(30, 30, 30));
		opt2.setFont(new Font("Normal", Font.PLAIN, 18));
		CloseButtonListener no = new CloseButtonListener(this, opt2);
		opt2.addActionListener(no);
		opt2.setVisible(false);
		this.add(opt2);

		this.setLayout(null);
		this.setVisible(false);
	}

	public void gameEnd(String end) {
		frame.endPanel.setVisible(true);
		frame.combatPanel.setVisible(false);
		frame.floorPanel.setVisible(false);
		frame.textPanel.setVisible(false);
		frame.help.setVisible(false);
		frame.floor.setText("");
		heading.setLocation(80, 30);
		Label floor = new Label();
		floor.setBounds(530, 240, 200, 30);
		floor.setForeground(new Color(255, 255, 255));
		floor.setFont(new Font("Normal", Font.ITALIC, 20));
		floor.setAlignment(Label.CENTER);
		this.add(floor);
		if (end.equals("-")) {
			heading.setText("YOU DIED");
			floor.setText("Died on floor: " + frame.floorPanel.floor.getFloor());
		} else if (end.equals("+")) {
			heading.setText("YOU DEFEATED THE KINGS OF HELL");
			floor.setText("Conquered HELL");
		}

		Label name = new Label(frame.floorPanel.floor.hero.getName());
		name.setBounds(530, 120, 200, 30);
		name.setForeground(new Color(255, 255, 255));
		name.setFont(new Font("Normal", Font.ITALIC, 20));
		name.setAlignment(Label.CENTER);
		this.add(name);

		Label gender = new Label(frame.floorPanel.floor.hero.getGender());
		gender.setBounds(530, 150, 200, 30);
		gender.setForeground(new Color(255, 255, 255));
		gender.setFont(new Font("Normal", Font.ITALIC, 20));
		gender.setAlignment(Label.CENTER);
		this.add(gender);

		Label race = new Label(frame.floorPanel.floor.hero.getRace());
		race.setBounds(530, 180, 200, 30);
		race.setForeground(new Color(255, 255, 255));
		race.setFont(new Font("Normal", Font.ITALIC, 20));
		race.setAlignment(Label.CENTER);
		this.add(race);

		Label level = new Label("Level: " + frame.floorPanel.floor.hero.getLevel());
		level.setBounds(530, 210, 200, 30);
		level.setForeground(new Color(255, 255, 255));
		level.setFont(new Font("Normal", Font.ITALIC, 20));
		level.setAlignment(Label.CENTER);
		this.add(level);

		// TODO add pfps for all races 200x300
		heroPfp = new File("art/pfps/" + frame.floorPanel.floor.hero.getRace().toLowerCase().replaceAll(" ", "_")
				+ frame.floorPanel.floor.hero.getGender().toUpperCase() + ".png");
		this.repaint();
	}

	public void gameClose() {
		frame.floorPanel.setVisible(false);
		frame.endPanel.setVisible(true);
		frame.combatPanel.setVisible(false);
		frame.textPanel.setVisible(false);
		frame.help.setVisible(false);
		frame.helpPanel.setVisible(false);
		frame.floor.setVisible(false);

		heading.setText("Do you want to save?");

		opt1.setVisible(true);

		opt2.setVisible(true);
	}

	public void paint(Graphics g) {
		if (heroPfp != null) {
			try {
				img = ImageIO.read(heroPfp);

				g.setColor(new Color(130, 130, 130));
				g.fillRect(50, 80, 200, 300);
				
				g.setColor(frame.floorPanel.floor.hero.getEyeColor());
				g.fillRect(127, 133, 23, 15);
				
				g.drawImage(img, 50, 80, null);

				g.setColor(new Color(255, 255, 255));
				g.drawRect(50, 80, 200, 300);

				g.setColor(new Color(50, 50, 50));
				g.fillRect(280, 100, 240, 200);

				int[] stats = { frame.floorPanel.floor.hero.getSTA(), frame.floorPanel.floor.hero.getSTR(),
						frame.floorPanel.floor.hero.getDEX(), frame.floorPanel.floor.hero.getDEF(),
						frame.floorPanel.floor.hero.getINT(), frame.floorPanel.floor.hero.getMAS(),
						frame.floorPanel.floor.hero.getCHA() };

				int total = stats[0] + stats[1] + stats[2] + stats[3] + stats[4] + stats[5] + stats[6];

				g.setColor(new Color(255, 255, 255));

				for (int i = 0; i < stats.length; i++) {
					switch (i) {
					case 0:
						g.drawString("STA", 300 + i * 30, 315);
						g.setColor(new Color(0, 102, 0));
						break;
					case 1:
						g.drawString("STR", 300 + i * 30, 315);
						g.setColor(new Color(204, 0, 0));
						break;
					case 2:
						g.drawString("DEX", 300 + i * 30, 315);
						g.setColor(new Color(255, 217, 25));
						break;
					case 3:
						g.drawString("DEF", 300 + i * 30, 315);
						g.setColor(new Color(0, 230, 191));
						break;
					case 4:
						g.drawString("INT", 300 + i * 30, 315);
						g.setColor(new Color(153, 238, 255));
						break;
					case 5:
						g.drawString("MAS", 300 + i * 30, 315);
						g.setColor(new Color(145, 145, 145));
						break;
					case 6:
						g.drawString("CHA", 300 + i * 30, 315);
						g.setColor(new Color(225, 77, 255));
						break;
					}
					g.fillRect(300 + i * 30, 300 - (int) (stats[i] * 100 / total) * 2, 20,
							(int) (stats[i] * 100 / total) * 2);

					g.setColor(new Color(255, 255, 255));
				}

				g.drawRect(280, 100, 240, 200);
			} catch (IOException e) {
				img = null;
			}
		}

		g.setColor(new Color(255, 255, 255));
		g.drawRect(0, 0, 760, 420);
	}
}
