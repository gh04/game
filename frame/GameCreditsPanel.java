package frame;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.util.LinkedList;
import java.util.List;

import listeners.CreditsButtonListener;

public class GameCreditsPanel extends Panel {
	public GameFrame frame;
	List<Label> text = new LinkedList<Label>();
	Button back;

	GameCreditsPanel(GameFrame f) {
		frame = f;
		this.setBounds(0, 0, frame.getWidth(), frame.getHeight());
		this.setBackground(new Color(20, 20, 20));

		back = new Button("BACK");
		back.setBounds(360, 400, 100, 30);
		back.setFont(new Font("Normal", Font.PLAIN, 18));
		back.setForeground(new Color(255, 255, 255));
		back.setBackground(new Color(30, 30, 30));
		CreditsButtonListener cbl = new CreditsButtonListener(this);
		back.addActionListener(cbl);
		this.add(back);

		text.add(new Label("CREDITS"));
		text.get(0).setBounds(360, 100, 100, 30);
		text.get(0).setFont(new Font("Bold", Font.BOLD, 22));
		text.get(0).setForeground(new Color(255, 255, 255));
		text.get(0).setAlignment(Label.CENTER);
		this.add(text.get(0));
		
		text.add(new Label("Game Developer & Designer:"));
		text.get(1).setBounds(260, 150, 300, 30);
		text.get(1).setFont(new Font("Normal", Font.PLAIN, 20));
		text.get(1).setForeground(new Color(255, 255, 255));
		text.get(1).setAlignment(Label.CENTER);
		this.add(text.get(1));
		
		text.add(new Label("Gavril Mihailov Hristov"));
		text.get(2).setBounds(260, 190, 300, 30);
		text.get(2).setFont(new Font("Normal", Font.ITALIC, 20));
		text.get(2).setForeground(new Color(255, 255, 255));
		text.get(2).setAlignment(Label.CENTER);
		this.add(text.get(2));
		
		text.add(new Label("Artist:"));
		text.get(3).setBounds(260, 230, 300, 30);
		text.get(3).setFont(new Font("Normal", Font.PLAIN, 20));
		text.get(3).setForeground(new Color(255, 255, 255));
		text.get(3).setAlignment(Label.CENTER);
		this.add(text.get(3));
		
		text.add(new Label("Daniel Iordanov Sotirov"));
		text.get(4).setBounds(260, 270, 300, 30);
		text.get(4).setFont(new Font("Normal", Font.ITALIC, 20));
		text.get(4).setForeground(new Color(255, 255, 255));
		text.get(4).setAlignment(Label.CENTER);
		this.add(text.get(4));

		this.setLayout(null);
		this.setVisible(false);
	}
}
