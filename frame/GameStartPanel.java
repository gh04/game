package frame;

import listeners.*;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

public class GameStartPanel extends Panel {
	public GameFrame frame;
	public Button start;
	public Button load;
	public Label credits;
	public TextField input;
	public List<Button> options = new LinkedList<Button>();
	public Label question;
	BufferedImage img;
	Font normal = new Font("Normal", Font.PLAIN, 18);
	Font bold = new Font("Bold", Font.BOLD, 20);

	GameStartPanel(GameFrame f) {
		frame = f;
		this.setBounds(0, 0, frame.getWidth(), frame.getHeight());
		this.setBackground(new Color(40, 40, 40));
		
		GameMouseListener gml = new GameMouseListener(this);
		this.addMouseListener(gml);

		start = new Button("START");
		start.setBounds(360, 250, 100, 30);
		start.setFont(normal);
		start.setForeground(new Color(255, 255, 255));
		start.setBackground(new Color(30, 30, 30));
		StartButtonListener sbl = new StartButtonListener(this);
		start.addActionListener(sbl);
		this.add(start);

		load = new Button("LOAD");
		load.setBounds(360, 300, 100, 30);
		load.setFont(normal);
		load.setForeground(new Color(255, 255, 255));
		load.setBackground(new Color(30, 30, 30));
		LoadButtonListener lbl = new LoadButtonListener(this);
		load.addActionListener(lbl);
		this.add(load);

		input = new TextField();
		input.setBounds(310, 230, 200, 30);
		input.setFont(new Font("Normal", Font.PLAIN, 20));
		input.setForeground(new Color(255, 255, 255));
		input.setBackground(new Color(30, 30, 30));
		NameInputKeyListener sikl = new NameInputKeyListener(this);
		input.addKeyListener(sikl);
		this.add(input);
		input.setVisible(false);

		question = new Label("What is your hero's name?");
		question.setBounds(100, 140, 620, 35);
		question.setFont(new Font("Normal", Font.ITALIC, 30));
		question.setForeground(new Color(255, 255, 255));
		question.setAlignment(Label.CENTER);
		this.add(question);
		question.setVisible(false);

		this.setLayout(null);
		this.setVisible(true);
	}

	public void clearOptions() {
		for (Button b : options) {
			this.remove(b);
		}
		options.clear();
	}

	public void colorOptions() {
		StartOptionButtonListener sobl;
		int r = 0, g = 0, b = 0;
		for (int i = 0; i < 65; i++) {
			r = (int) (Math.sin(0.095 * i + 2) * 127 + 128);
			g = (int) (Math.sin(0.095 * i + 4) * 127 + 128);
			b = (int) (Math.sin(0.095 * i + 6) * 127 + 128);

			options.add(new Button());
			options.get(i).setBackground(new Color(r, g, b));
			sobl = new StartOptionButtonListener(this, options.get(i));
			options.get(i).addActionListener(sobl);
			this.add(options.get(i));
		}

		for (int y = 190, i = 0; y <= 410; y += 50) {
			for (int x = 90; x <= 700; x += 50, i++) {
				options.get(i).setBounds(x, y, 40, 40);
			}
		}
	}

	public void paint(Graphics g) {
		if (this.start.isVisible() == true) {
			try {
				img = ImageIO.read(new File("art/main.png"));
			} catch (IOException e) {
				img = null;
			}
			g.drawImage(img, 0, 0, null);
			g.setColor(new Color(255, 255, 255));
			g.drawString("Credits", 20, 40);
		} else {
			try {
				img = ImageIO.read(new File("art/start.png"));
			} catch (IOException e) {
				img = null;
			}
			g.drawImage(img, 0, 0, null);
		}
	}
}
