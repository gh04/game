package frame;

import listeners.*;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

public class GameHelpPanel extends Panel {
	public GameFrame frame;
	Label heading;
	public List<Label> label = new LinkedList<Label>();
	Font fontHeading = new Font("Bold", Font.BOLD, 24);
	Font fontText = new Font("Normal", Font.PLAIN, 16);
	public Button stats;
	public Button leveling;
	public Button combatInfo;
	public Button raceInfo;
	public Button exit;
	public Button back;
	BufferedImage img;

	GameHelpPanel(GameFrame f) {
		frame = f;
		this.setBounds(30, 50, 761, 421);
		this.setBackground(new Color(40, 40, 40));

		heading = new Label("HELP");
		heading.setBounds(280, 15, 200, 30);
		heading.setForeground(new Color(255, 255, 255));
		heading.setFont(fontHeading);
		heading.setAlignment(Label.CENTER);
		this.add(heading);

		stats = new Button("STATS");
		stats.setBounds(50, 140, 120, 30);
		stats.setBackground(new Color(30, 30, 30));
		stats.setForeground(new Color(255, 255, 255));
		stats.setFont(fontText);
		StatsButtonListener sbl = new StatsButtonListener(this);
		stats.addActionListener(sbl);
		this.add(stats);

		leveling = new Button("LEVELING");
		leveling.setBounds(230, 140, 120, 30);
		leveling.setBackground(new Color(30, 30, 30));
		leveling.setForeground(new Color(255, 255, 255));
		leveling.setFont(fontText);
		LevelButtonListener lbl = new LevelButtonListener(this);
		leveling.addActionListener(lbl);
		this.add(leveling);

		combatInfo = new Button("COMBAT");
		combatInfo.setBounds(410, 140, 120, 30);
		combatInfo.setBackground(new Color(30, 30, 30));
		combatInfo.setForeground(new Color(255, 255, 255));
		combatInfo.setFont(fontText);
		CombatInfoButtonListener cbl = new CombatInfoButtonListener(this);
		combatInfo.addActionListener(cbl);
		this.add(combatInfo);

		raceInfo = new Button("RACE INFO");
		raceInfo.setBounds(590, 140, 120, 30);
		raceInfo.setBackground(new Color(30, 30, 30));
		raceInfo.setForeground(new Color(255, 255, 255));
		raceInfo.setFont(fontText);
		RaceInfoButtonListener ribl = new RaceInfoButtonListener(this);
		raceInfo.addActionListener(ribl);
		this.add(raceInfo);

		exit = new Button("EXIT");
		exit.setBounds(590, 340, 120, 30);
		exit.setBackground(new Color(30, 30, 30));
		exit.setForeground(new Color(255, 255, 255));
		exit.setFont(fontText);
		ExitButtonListener ebl = new ExitButtonListener(this.frame);
		exit.addActionListener(ebl);
		this.add(exit);

		back = new Button("BACK");
		back.setBounds(590, 340, 120, 30);
		back.setBackground(new Color(30, 30, 30));
		back.setForeground(new Color(255, 255, 255));
		back.setFont(fontText);
		BackButtonListener bbl = new BackButtonListener(this);
		back.addActionListener(bbl);
		this.add(back);
		back.setVisible(false);

		this.setLayout(null);
		this.setVisible(false);
	}

	public void setHeading(String head) {
		heading.setText(head);
	}// heading

	public void setText(String text, int pos) {
		label.add(new Label(text));
		label.get(pos).setBounds(50, 70 + 30 * pos, 660, 30);
		label.get(pos).setForeground(new Color(255, 255, 255));
		label.get(pos).setFont(fontText);
		label.get(pos).setAlignment(Label.LEFT);
		this.add(label.get(pos));
	}// text

	public void clearText() {
		for (int i = 0; i < label.size(); i++) {
			this.remove(label.get(i));
		}
		label.clear();
	}// clear text

	public void paint(Graphics g) {
		try {
			img = ImageIO.read(new File("art/help.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(img, 0, 0, null);
		g.setColor(new Color(255, 255, 255));
		g.drawRect(0, 0, 760, 420);
	}// border
}
