package frame;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import entity.Passive;
import listeners.SkillBackButtonListener;

public class GameSkillPanel extends Panel {
	public GameFrame frame;
	public Button back;
	private List<Label> label = new LinkedList<Label>();
	Label heading;

	GameSkillPanel(GameFrame f) {
		frame = f;
		this.setBounds(490, 50, 301, 421);
		this.setBackground(new Color(40, 40, 40));

		heading = new Label("SKILL");
		heading.setBounds(105, 20, 90, 30);
		heading.setForeground(new Color(255, 255, 255));
		heading.setFont(new Font("Bold", Font.BOLD, 20));
		heading.setAlignment(Label.CENTER);
		this.add(heading);

		for (int i = 0; i < 7; i++) {
			label.add(new Label());
			label.get(i).setBounds(20, 70 + 30 * i, 260, 30);
			label.get(i).setForeground(new Color(255, 255, 255));
			label.get(i).setAlignment(Label.CENTER);
			this.add(label.get(i));
		}
		label.get(0).setFont(new Font("Bold", Font.BOLD, 18));
		label.get(1).setFont(new Font("Normal", Font.PLAIN, 18));

		back = new Button("BACK");
		back.setBounds(105, 370, 100, 30);
		back.setBackground(new Color(30, 30, 30));
		back.setForeground(new Color(255, 255, 255));
		back.setFont(new Font("Normal", Font.PLAIN, 18));
		SkillBackButtonListener sbbl = new SkillBackButtonListener(this);
		back.addActionListener(sbbl);
		this.add(back);

		this.setLayout(null);
		this.setVisible(false);
	}

	public void skillInfo(String skill) {

		label.get(0).setText("< " + skill + " >");

		switch (skill) {
		case "Hit": // deals 100% str dmg
			label.get(2).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You hit your enemy");
			label.get(2).setText("Deals 100% of your STR");
			label.get(3).setText("");
			label.get(4).setText("");
			label.get(5).setText("");
			label.get(6).setText("");
			break;

		case "Demonic Trance": // str+2 dex+2 def+1 cha+2
			label.get(2).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(6).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("Your lust for blood surges");
			label.get(2).setText("Gain a status boost");
			label.get(3).setText("STR + 2");
			label.get(4).setText("DEX + 2");
			label.get(5).setText("DEF + 1");
			label.get(6).setText("CHA + 2");
			break;

		case "Blood Lad": // deals 50% currentHP damage and heals for that amount
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You sink your teeth");
			label.get(2).setText("into your enemy");
			label.get(3).setText("Deals 50% of your Health");
			label.get(4).setText("Heals for the same amount");
			label.get(5).setText("");
			label.get(6).setText("");
			break;

		case "Blood Dance": // deals 100% maxHP damage
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You dash around the area");
			label.get(2).setText("spreading blood all over");
			label.get(3).setText("Deals 100%  of your STA");
			label.get(4).setText("");
			label.get(5).setText("");
			label.get(6).setText("");
			break;

		case "Phase": // deals 100% str + 50% dex damage
			label.get(2).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You dash at your enemy");
			label.get(2).setText("Deals 100% of your STR");
			label.get(3).setText("+ 50% of your DEX");
			label.get(4).setText("");
			label.get(5).setText("");
			label.get(6).setText("");
			break;

		case "Possess": // deals dex% of enemy maxHP
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You take over your");
			label.get(2).setText("enemy's body, making");
			label.get(3).setText("them attack themselves");
			label.get(4).setText("Deals DEX% of enemy HP");
			label.get(5).setText("");
			label.get(6).setText("");
			break;

		case "Smash": // deals 100% str + 20% def damage
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You strike the ground");
			label.get(2).setText("Deals 100% of your STR");
			label.get(3).setText("+ 20% of your DEF");
			label.get(4).setText("");
			label.get(5).setText("");
			label.get(6).setText("");
			break;

		case "Rough": // def+5 str+2 mas+3 heals 3hp
			label.get(2).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(6).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("Your skin hardens");
			label.get(2).setText("Gain a status boost");
			label.get(3).setText("STR + 2");
			label.get(4).setText("DEF + 5");
			label.get(5).setText("MAS + 3");
			label.get(6).setText("Heals 3 Health");
			break;
		case "Arcane Blast": // deals 200% int damage
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You shoot a blast of");
			label.get(2).setText("magical energy");
			label.get(3).setText("Deals 200% of your INT");
			label.get(4).setText("");
			label.get(5).setText("");
			label.get(6).setText("");
			break;
		case "Deviant": // int+10 def+2 mas+6
			label.get(2).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(6).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You embrace your madness");
			label.get(2).setText("Gain a status boost");
			label.get(3).setText("INT + 10");
			label.get(4).setText("DEF + 2");
			label.get(5).setText("MAS + 6");
			label.get(6).setText("");
			break;
		case "Berserk": // str+4 dex+5 heals for 20%hp if under 50%
			label.get(2).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(6).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You go into a blind rage");
			label.get(2).setText("Gain a status boost");
			label.get(3).setText("STR + 4");
			label.get(4).setText("DEX + 5");
			label.get(5).setText("If you are under 50% Health,");
			label.get(6).setText("Heals for 20% of Health");
			break;
		case "Devour": // deals enemyHP% str damage
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You ferociously attack");
			label.get(2).setText("dealing immense damage");
			label.get(3).setText("Deals 100% of STR");
			label.get(4).setText("The less health the enemy has");
			label.get(5).setText("the more damage they take");
			label.get(6).setText("");
			break;
		case "True Slash": // deals 100% str damage (if it crits deals 100% maxHP)
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(6).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You strike your enemy with");
			label.get(2).setText("upmost precision");
			label.get(3).setText("Deals 100% of STR");
			label.get(4).setText("If this skill crits,");
			label.get(5).setText("Deal 100% of enemy Health");
			label.get(6).setText("(Does not ignore DEF)");
			break;
		case "Dead Calm": // mas+=15 str+=2 def+=3
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(6).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You enter a state of");
			label.get(2).setText("complete concentration");
			label.get(3).setText("Gain a status boost");
			label.get(4).setText("STR + 2");
			label.get(5).setText("DEF + 3");
			label.get(6).setText("MAS + 15");
			break;
		case "Charm": // deals 100% int and 20% cha damage, if crit - heals for 50% cha dmg
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(6).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You use your allure to");
			label.get(2).setText("charm your foe");
			label.get(3).setText("Deals 100% of INT");
			label.get(4).setText("+ 20% of CHA");
			label.get(5).setText("If this skill crits,");
			label.get(6).setText("Heal for 50% of CHA");
			break;
		case "Man Eater": // deals a guaranteed crit of 120% int damage(base crit damage 200%)
			label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
			label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
			label.get(1).setText("You strike your already enticed");
			label.get(2).setText("target in their weakest spot");
			label.get(3).setText("Deals 120% of INT");
			label.get(4).setText("A crit is guaranteed");
			label.get(5).setText("");
			label.get(6).setText("");
			break;
		case "Passive":
			label.get(0).setText("[ " + frame.floorPanel.floor.hero.getPassive() + " ]");
			switch (frame.floorPanel.floor.hero.getPassive()) {
			case Bloodlust: // get random stat boost each turn
				label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
				label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(1).setText("Your fervor for blood");
				label.get(2).setText("increases as the battle goes on");
				label.get(3).setText("Random stat increase each turn");
				label.get(4).setText("");
				label.get(5).setText("");
				label.get(6).setText("");
				break;
			case Forgotten: // 20% chance to deal 40% DEX damage
				label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
				label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(1).setText("Your existence has been");
				label.get(2).setText("long erased of the past");
				label.get(3).setText("You have a small chance to");
				label.get(4).setText("deal extra True Damage");
				label.get(5).setText("");
				label.get(6).setText("");
				break;
			case Brute: // while HP>50%, STR+3
				label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
				label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(1).setText("Your beastly strength");
				label.get(2).setText("is at its peak");
				label.get(3).setText("If your Health is above 50%,");
				label.get(4).setText("increase your STR by 3");
				label.get(5).setText("");
				label.get(6).setText("");
				break;
			case Wise: // enemy stats decrease each round
				label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
				label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(1).setText("Your wisdom gives you");
				label.get(2).setText("the edge in any battle");
				label.get(3).setText("Enemy stats decrease");
				label.get(4).setText("each round");
				label.get(5).setText("");
				label.get(6).setText("");
				break;
			case Ferocious: // heal for 1 each round
				label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
				label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(1).setText("Your voraciousness makes you");
				label.get(2).setText("far tougher to defeat");
				label.get(3).setText("You regenerate health");
				label.get(4).setText("each round");
				label.get(5).setText("");
				label.get(6).setText("");
				break;
			case Unyielding: // when under 50% HP heal to 70% and STR + 4
				label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
				label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(5).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(1).setText("Your devotion to combat");
				label.get(2).setText("never falters");
				label.get(3).setText("If Health drops under");
				label.get(4).setText("50%, heals to 70% and");
				label.get(5).setText("STR increases by 4");
				label.get(6).setText("");
				break;
			case Lustful: // the less HP of enemy, the stronger
				label.get(2).setFont(new Font("Normal", Font.PLAIN, 18));
				label.get(3).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(4).setFont(new Font("Italic", Font.ITALIC, 17));
				label.get(1).setText("Your thirst for power");
				label.get(2).setText("increases as your lust does");
				label.get(3).setText("The weaker the enemy is,");
				label.get(4).setText("the stronger you get");
				label.get(5).setText("");
				label.get(6).setText("");
				break;
			}
			break;
		}
	}

	public void paint(Graphics g) {
		BufferedImage img;
		try {
			img = ImageIO.read(new File("art/textBorder.png"));
		} catch (IOException e) {
			img = null;
		}
		g.drawImage(img, 0, 0, null);
		g.setColor(new Color(255, 255, 255));
		g.drawRect(0, 0, 300 - 1, 420 - 1);
	}

	public List<Label> getLabel() {
		return label;
	}

	public void setLabel(List<Label> l) {
		label = l;
	}
}
