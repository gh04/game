package frame;

import game.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;

public class GameFloorPanel extends Panel {
	public Floor floor;

	GameFloorPanel(Floor f) {
		floor = f;
		this.setBounds(30, 50, 421, 421);
		this.setBackground(new Color(40, 40, 40));
		this.setLayout(null);

		this.setVisible(false);
	}

	public synchronized void paint(Graphics g) {
		g.drawImage(floor.getImg(), 0, 0, null);

		for (int i = 0; i < floor.getSizeY(); i++) {
			for (int j = 0; j < floor.getSizeX(); j++) {
				if (floor.hero.getPosX() == j && floor.hero.getPosY() == i) {
					g.drawImage(floor.hero.getImg(), 20 * j, 20 * i, null);
				} else if (floor.boss.getPosX() == j && floor.boss.getPosY() == i) {
					g.drawImage(floor.boss.getImg(), 20 * j - 5, 20 * i - 5, null);
				}
				for (int p = 0; p < floor.enemies.size(); p++) {
					if (floor.enemies.get(p).getPosX() == j && floor.enemies.get(p).getPosY() == i) {
						g.drawImage(floor.enemies.get(p).getImg(), 20 * j, 20 * i, null);
						break;
					}
				}
			}
		}

		g.setColor(new Color(255, 255, 255));
		g.drawRect(0, 0, 20 * floor.getSizeX() - 1, 20 * floor.getSizeY() - 1);
	}

}
