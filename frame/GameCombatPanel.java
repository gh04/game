package frame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.util.LinkedList;
import java.util.List;

public class GameCombatPanel extends Panel {
	GameFrame frame;
	public List<Label> enemy = new LinkedList<Label>();
	public List<Label> hero = new LinkedList<Label>();
	Font normal = new Font("Normal", Font.PLAIN, 18);
	Font bold = new Font("Bold", Font.BOLD, 20);

	GameCombatPanel(GameFrame f) {
		frame = f;

		this.setLocation(30, 50);
		this.setSize(421, 421);
		this.setBackground(new Color(40, 40, 40));

		for (int i = 0; i < 8; i++) {
			enemy.add(new Label());
			enemy.get(i).setBounds(220, 30 + 30 * i, 190, 30);
			enemy.get(i).setForeground(new Color(255, 255, 255));
			enemy.get(i).setFont(normal);
			enemy.get(i).setAlignment(Label.CENTER);
			this.add(enemy.get(i));
		}
		
		enemy.add(new Label());
		enemy.get(8).setBounds(224, 304, 182, 22);
		enemy.get(8).setVisible(false);
		this.add(enemy.get(8));
		
		for (int i = 0; i < 8; i++) {
			hero.add(new Label());
			hero.get(i).setBounds(10, 30 + 30 * i, 190, 30);
			hero.get(i).setForeground(new Color(255, 255, 255));
			hero.get(i).setFont(normal);
			hero.get(i).setAlignment(Label.CENTER);
			this.add(hero.get(i));
		}
		
		hero.add(new Label());
		hero.get(8).setBounds(14, 304, 182, 22);
		hero.get(8).setBackground(Color.YELLOW);
		hero.get(8).setVisible(false);
		this.add(hero.get(8));
		
		this.setLayout(null);
		this.setVisible(false);
	}

	public synchronized void paint(Graphics g) {
		g.setColor(new Color(255, 255, 255));
		g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
		g.drawLine(210, 0, 210, 420);

		for (int i = 0; i < frame.floorPanel.floor.hero.getCurrentHP(); i++) {
			g.fillRect(16 + i * 180 / frame.floorPanel.floor.hero.getMaxHP(), 306,
					180 / frame.floorPanel.floor.hero.getMaxHP() - 1, 18);
		}
		g.drawRect(14, 304, 182, 22);

		if (frame.textPanel.getEnemyType().equals("enemy")
				&& frame.command.combat.getEnemies().get(frame.command.combat.getPos()).getCurrentHP() >= 0) {
			for (int i = 0; i < frame.command.combat.getEnemies().get(frame.command.combat.getPos())
					.getCurrentHP(); i++) {
				g.fillRect(
						226 + i * 180 / frame.command.combat.getEnemies().get(frame.command.combat.getPos()).getMaxHP(),
						306, 180 / frame.command.combat.getEnemies().get(frame.command.combat.getPos()).getMaxHP() - 1,
						18);
			}
		} else if (frame.textPanel.getEnemyType().equals("boss")
				&& frame.command.combat.getBoss().getCurrentHP() >= 0) {
			for (int i = 0; i < frame.command.combat.getBoss().getCurrentHP(); i++) {
				g.fillRect(226 + i * 180 / frame.command.combat.getBoss().getMaxHP(), 306,
						180 / frame.command.combat.getBoss().getMaxHP() - 1, 18);
			}
		}
		g.drawRect(224, 304, 182, 22);
	}

}
