package game;

import entity.*;
import frame.*;

import java.awt.Font;
import java.io.*;

public class Command {
	BufferedReader sc;
	public GameFrame frame;
	Help hp = new Help();
	public Combat combat;

	Command(BufferedReader scan, GameFrame gf, Combat cmb) {
		sc = scan;
		frame = gf;
		combat = cmb;
	}

	public synchronized void readCommand(String c) {
		if (c.equals("w") || c.equals("s") || c.equals("a") || c.equals("d")) {
			frame.floorPanel.floor.hero.move(c, frame.floorPanel.floor);
			combat.enemyCheck(sc);
			frame.floorPanel.repaint();
		} // movement

		else if (c.equals("info")) {
			frame.textPanel.clearAll();
			seeInfo();
		} // info

		else if (c.equals("stats")) {
			frame.textPanel.clearAll();
			if (frame.floorPanel.floor.hero.getSP() > 0) {
				frame.textPanel.statPointButtons();
			}
			seeStats();
		} // stats

		else if (c.equals("help")) {
			seeHelp();
		} // help

		else if (c.equals("rinf")) {
			hp.raceInfo(frame.floorPanel.floor.hero);
		} // race info

		else if (c.equals("spuse")) {
			if (frame.floorPanel.floor.hero.getSP() > 0) {
				frame.floorPanel.floor.hero.pointUse();
			} else
				System.out.println("You have no stat points to spend.");
		} // use stat points

		else if (c.equals("skills")) {
			frame.textPanel.clearAll();
			seeSkills();
		}

		else if (c.equals("save")) {
			save();
		} // save

		else if (c.equals("lup") && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.floorPanel.floor.hero.setXP(frame.floorPanel.floor.hero.getLevel() * 10);
			frame.floorPanel.floor.hero.lvlUP();
			frame.floorPanel.floor.hero.changeImage();
		} // instant level up - cheat

		else if (c.equals("xp") && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.floorPanel.floor.hero.setXP(frame.floorPanel.floor.hero.getXP() + 10);
			frame.floorPanel.floor.hero.lvlUP();
			frame.floorPanel.floor.hero.changeImage();
		} // instant 10 xp - cheat

		else if (c.equals("lv4") && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.floorPanel.floor.hero.setLevel(4);
			frame.floorPanel.floor.hero
					.setSP(frame.floorPanel.floor.hero.getSP() + 4 - frame.floorPanel.floor.hero.getSP());
		} // instant lvl 4 - cheat

		else if (c.equals("lv9") && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.floorPanel.floor.hero.setLevel(9);
			frame.floorPanel.floor.hero
					.setSP(frame.floorPanel.floor.hero.getSP() + 9 - frame.floorPanel.floor.hero.getSP());
		} // instant lvl 9 - cheat

		else if (c.equals("lv19") && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.floorPanel.floor.hero.setLevel(19);
			frame.floorPanel.floor.hero
					.setSP(frame.floorPanel.floor.hero.getSP() + 19 - frame.floorPanel.floor.hero.getSP());
		} // instant lvl 19 - cheat

		else if (c.equals("lv29") && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.floorPanel.floor.hero.setLevel(29);
			frame.floorPanel.floor.hero
					.setSP(frame.floorPanel.floor.hero.getSP() + 29 - frame.floorPanel.floor.hero.getSP());
		} // instant lvl 29 - cheat

		else if (c.equals("nextf") && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.floorPanel.floor.nextFloor();
			if (frame.floorPanel.floor.getFloor() != 8) {
				frame.floor.setText("Floor " + frame.floorPanel.floor.getFloor());
			} else {
				frame.floor.setText("Floor ???");
			}
			frame.floorPanel.floor.changeImage();
		} // instant floor up - cheat

		else if ((c.equals("kill") || c.equals("die")) && frame.floorPanel.floor.hero.isAdmin() == true) {
			frame.endPanel.gameEnd("-");
		} // insta death - cheat

		else if (c.equals("exit")) {
			System.out.println("Do you want to save? Yes / No");
			while (true) {
				try {
					c = sc.readLine().toLowerCase();
				} catch (IOException e1) {
					c = "";
				}
				if (c.equals("yes")) {
					save();
					System.exit(0);
					break;
				} else if (c.equals("no")) {
					System.exit(0);
					break;
				} else
					System.out.println("Invalid input. Try again.");
			}
		} // exit

		else
			System.out.println("Invalid input.");
	}

	public void save() {
		try {
			FileOutputStream fos = new FileOutputStream("save.bin");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(frame.floorPanel.floor);
			System.out.println("Game Saved");
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void seeInfo() {
		frame.textPanel.clearAll();
		frame.textPanel.setHeading("INFO");
		frame.textPanel.setText("Name: " + frame.floorPanel.floor.hero.getName(), 0);
		frame.textPanel.setText(
				"Health: " + frame.floorPanel.floor.hero.getCurrentHP() + "/" + frame.floorPanel.floor.hero.getMaxHP(),
				1);
		frame.textPanel.setText("Gender: " + frame.floorPanel.floor.hero.getGender(), 2);
		frame.textPanel.setText("Race: " + frame.floorPanel.floor.hero.getRace(), 3);
		frame.textPanel.setText("Level: " + frame.floorPanel.floor.hero.getLevel(), 4);
		frame.textPanel.setText("XP: ", 5);
		frame.textPanel.setText("", 6);
		frame.textPanel.getLabel().get(6).setVisible(false);
		frame.textPanel.setText("Stat Points: " + frame.floorPanel.floor.hero.getSP(), 7);
		frame.textPanel.infoSwitch.setLabel("STATS");
	}

	public void seeStats() {
		frame.textPanel.setHeading("STATS");
		frame.textPanel.setText("STA: " + frame.floorPanel.floor.hero.getSTA(), 0);
		frame.textPanel.setText("STR: " + frame.floorPanel.floor.hero.getSTR(), 1);
		frame.textPanel.setText("DEX: " + frame.floorPanel.floor.hero.getDEX(), 2);
		frame.textPanel.setText("DEF: " + frame.floorPanel.floor.hero.getDEF(), 3);
		frame.textPanel.setText("INT: " + frame.floorPanel.floor.hero.getINT(), 4);
		frame.textPanel.setText("MAS: " + frame.floorPanel.floor.hero.getMAS(), 5);
		frame.textPanel.setText("CHA: " + frame.floorPanel.floor.hero.getCHA(), 6);
		frame.textPanel.setText("Stat Points: " + frame.floorPanel.floor.hero.getSP(), 7);
		frame.textPanel.infoSwitch.setLabel("SKILLS");
	}

	public void seeSkills() {
		frame.textPanel.clearAll();
		frame.textPanel.setHeading("SKILLS");
		frame.textPanel.skillButtons();
	}

	public void seeHelp() {
		frame.textPanel.setVisible(false);
		frame.floorPanel.setVisible(false);
		frame.helpPanel.setVisible(true);
		frame.helpPanel.setHeading("HELP");
		frame.helpPanel.setText("What do you want help with?", 0);
		frame.helpPanel.label.get(0).setFont(new Font("Normal", Font.PLAIN, 22));
	}
}
