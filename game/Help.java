package game;

import java.io.*;
import java.util.*;
import entity.Hero;

public class Help {
	File help = new File("help.txt");
	File rinf = new File("races.txt");
	Scanner sc;

	public void stats() {
		System.out.println("~~~Help~~~");
		try {
			sc = new Scanner(help);
			for (int i = 0; i < 10; i++) {
				System.out.println(sc.nextLine());
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error when reading file: " + help.getName());
		}
	}

	public void leveling() {
		System.out.println("~~~Leveling~~~");
		try {
			sc = new Scanner(help);
			for (int i = 0; i < 17; i++) {
				if (i > 10)
					System.out.println(sc.nextLine());
				else
					sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.err.println("Error when reading file: " + help.getName());
		}
	}

	public void commands() {
		System.out.println("~~~Commands~~~");
		try {
			sc = new Scanner(help);
			for (int i = 0; i < 24; i++) {
				if (i > 16)
					System.out.println(sc.nextLine());
				else
					sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.err.println("Error when reading file: " + help.getName());
		}
	}

	public void raceInfo(Hero h) {
		switch (h.getRace()) {
		case "Cambion":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 5; i++) {
					System.out.println(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Lesser Demon":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 10; i++) {
					if (i >= 5)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Demon":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 17; i++) {
					if (i >= 10)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Dhampir":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 25; i++) {
					if (i >= 17)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Ghost":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 33; i++) {
					if (i >= 25)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Ogre":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 41; i++) {
					if (i >= 33)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Ghoul":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 49; i++) {
					if (i >= 41)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Beast":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 57; i++) {
					if (i >= 49)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Majin":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 65; i++) {
					if (i >= 57)
						System.out.println(sc.nextLine());
					else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when reading file: " + rinf.getName());
			}
			break;
		case "Imp":
//				try {
//					sc=new Scanner(rinf);
//					for(int i=0;i<73;i++) {
//						if(i>=65) System.out.println(sc.nextLine());
//						else sc.nextLine();
//					}
//					sc.close();
//				} catch (FileNotFoundException e) {
//					System.err.println("Error when reading file: "+rinf.getName());
//				}
//				break;
		default:
			System.out.format("Description for %f is not avalable yet.\n", h.getRace());
		}
	}

}
