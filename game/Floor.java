package game;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

import javax.imageio.ImageIO;

import entity.Boss;
import entity.Enemy;
import entity.Hero;

public class Floor implements Serializable {
	private static final long serialVersionUID = 1L;

	private int sizex = 21;
	private int sizey = 21;
	private int floor = 0;
	public Hero hero;
	public List<Enemy> enemies = new LinkedList<Enemy>();
	public Boss boss = new Boss(10, 10, "");
	transient private BufferedImage img;

	public Floor(int x, int y, int f) {
		sizex = x;
		sizey = y;
		floor = f;
	}

	public void print() {
		for (int i = 0; i < sizey; i++) {
			for (int j = 0; j < sizex; j++) {
				boolean entityExists = false;
				for (int p = 0; p < enemies.size(); p++) {
					if (hero.getPosX() == j && hero.getPosY() == i) {
						System.out.print("0 ");
						entityExists = true;
						break;
					} else if (boss.getPosX() == j && boss.getPosY() == i) {
						System.out.print("@ ");
						entityExists = true;
						break;
					} else if (enemies.get(p).getPosX() == j && enemies.get(p).getPosY() == i) {
						System.out.print("X ");
						entityExists = true;
						break;
					}
				}
				if (entityExists == false)
					System.out.print(". ");
			}
			System.out.println();
		}
		System.out.println();
	}// print

	public void nextFloor() {
		Random r = new Random();
		enemies.clear();
		floor++;
		hero.setPosX(0);
		hero.setPosY(0);
		switch (floor) {
		case 1:
			boss = new Boss(10, 10, "Belphegor");
			break;
		case 2:
			boss = new Boss(10, 10, "Mammon");
			break;
		case 3:
			boss = new Boss(10, 10, "Asmodeus");
			break;
		case 4:
			boss = new Boss(10, 10, "Leviathan");
			break;
		case 5:
			boss = new Boss(10, 10, "Beelzebub");
			break;
		case 6:
			boss = new Boss(10, 10, "Satan");
			break;
		case 7:
			boss = new Boss(10, 10, "Lucifer");
			break;
		case 8:
			boss = new Boss(10, 10, "Artemes");
			break;
		}

		if (floor != 8) {
			for (int i = 0; i < (7 + floor * 3); i++) {
				enemies.add(new Enemy(r.nextInt(0, sizex), r.nextInt(0, sizey), this.floor));

				while (((enemies.get(i).getPosX() >= 9 && enemies.get(i).getPosX() <= 11)
						&& (enemies.get(i).getPosY() >= 9 && enemies.get(i).getPosY() <= 11))
						|| (enemies.get(i).getPosX() == hero.getPosX() && enemies.get(i).getPosY() == hero.getPosY())) {
					enemies.get(i).setPosX(r.nextInt(0, sizex));
					enemies.get(i).setPosY(r.nextInt(0, sizex));
				}
				for (int j = 0; j < i; j++) {
					while (enemies.get(i).getPosX() == enemies.get(j).getPosX()
							&& enemies.get(i).getPosY() == enemies.get(j).getPosY()) {
						enemies.get(i).setPosX(r.nextInt(0, sizex));
						enemies.get(i).setPosY(r.nextInt(0, sizex));
					}
				}
			}
		}
	}// next floor

	public void changeImage() {
		try {
			this.setImg(ImageIO.read(new File("art/floor/floor" + this.getFloor() + ".png")));
		} catch (IOException e) {
			this.setImg(null);
		}
	}// change background

	public int getSizeX() {
		return sizex;
	}

	public void setSizeX(int x) {
		sizex = x;
	}

	public int getSizeY() {
		return sizey;
	}

	public void setSizeY(int y) {
		sizey = y;
	}// position

	public int getFloor() {
		return floor;
	}

	public void setFloor(int f) {
		floor = f;
	}// floor

	public BufferedImage getImg() {
		return img;
	}

	public void setImg(BufferedImage i) {
		img = i;
	}// img
}
