package game;

import entity.*;
import java.io.*;
import entity.Hero;
import frame.GameFrame;

public class Main {

	public static void main(String[] args) {
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		// hero
		Hero hero = new Hero(0, 0);
		hero.sc = sc;
		// map
		Floor floor = new Floor(21, 21, 0);
		floor.hero = hero;
		// frame
		GameFrame frame = new GameFrame(floor);
		// combat
		Combat cmb = new Combat(frame);
		cmb.sc = sc;
		// commands
		Command cmd = new Command(sc, frame, cmb);
		frame.command = cmd;
		hero.cmd = cmd;
		
		while (true) {
			String in;
			try {
				in = sc.readLine().toLowerCase();
			} catch (IOException e) {
				in = "";
			}
			cmd.readCommand(in);
		}
	}
}
