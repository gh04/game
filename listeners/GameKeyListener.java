package listeners;

import frame.GameFrame;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener {
	GameFrame frame;

	public GameKeyListener(GameFrame f) {
		frame = f;
	}

	public synchronized void keyTyped(KeyEvent e) {
		String c = ("" + e.getKeyChar()).toLowerCase();
		frame.command.readCommand(c);
	}

	public void keyPressed(KeyEvent e) {

	}

	public void keyReleased(KeyEvent e) {

	}
}
