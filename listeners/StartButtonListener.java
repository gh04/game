package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import frame.GameStartPanel;

public class StartButtonListener implements ActionListener {
	GameStartPanel startPanel;

	public StartButtonListener(GameStartPanel sp) {
		startPanel = sp;
	}

	public void actionPerformed(ActionEvent e) {
		startPanel.repaint();
		startPanel.question.setVisible(true);
		startPanel.input.setVisible(true);
		startPanel.start.setVisible(false);
		startPanel.load.setVisible(false);
	}

}
