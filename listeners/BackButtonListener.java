package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameHelpPanel;

public class BackButtonListener implements ActionListener {
	GameHelpPanel helpPanel;

	public BackButtonListener(GameHelpPanel h) {
		helpPanel = h;
	}

	public void actionPerformed(ActionEvent e) {
		helpPanel.back.setVisible(false);
		helpPanel.exit.setVisible(true);
		helpPanel.clearText();
		helpPanel.raceInfo.setVisible(true);
		helpPanel.leveling.setVisible(true);
		helpPanel.combatInfo.setVisible(true);
		helpPanel.stats.setVisible(true);
		helpPanel.frame.command.readCommand("help");
	}

}
