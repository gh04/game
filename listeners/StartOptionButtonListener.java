package listeners;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameStartPanel;

public class StartOptionButtonListener implements ActionListener{
	GameStartPanel startPanel;
	Button choice;
	
	public StartOptionButtonListener(GameStartPanel s, Button b){
		startPanel=s;
		choice=b;
	}
	
	
	public void actionPerformed(ActionEvent e) {
		if ((startPanel.question.getText().equals("What is your hero's gender?"))) {
			startPanel.frame.floorPanel.floor.hero.setGender(choice.getLabel().toLowerCase());
			startPanel.question.setText("What color are your hero's eyes?");
			startPanel.clearOptions();
			startPanel.colorOptions();
		} 
		
		else if (startPanel.question.getText().equals("What color are your hero's eyes?")) {
			startPanel.frame.floorPanel.floor.hero.setEyeColor(choice.getBackground());
			startPanel.clearOptions();
			startPanel.setVisible(false);
			startPanel.frame.floorPanel.setVisible(true);
			startPanel.frame.textPanel.setVisible(true);
			startPanel.frame.floorPanel.floor.nextFloor();
			startPanel.frame.floorPanel.floor.changeImage();
			startPanel.frame.command.readCommand("info");
		} 
	}

}
