package listeners;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import frame.GameStartPanel;

public class NameInputKeyListener implements KeyListener {
	// 10
	GameStartPanel startPanel;

	public NameInputKeyListener(GameStartPanel sp) {
		startPanel = sp;
	}

	public void keyTyped(KeyEvent e) {

	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == 10) {
			if (startPanel.input.getText().length() < 15 && !startPanel.input.getText().equals("")
					&& startPanel.question.getText().equals("What is your hero's name?")) {
				startPanel.frame.floorPanel.floor.hero.setName(startPanel.input.getText());
				startPanel.input.setVisible(false);
				startPanel.question.setText("What is your hero's gender?");

				startPanel.options.add(new Button("MALE"));
				startPanel.options.get(0).setBounds(190, 300, 120, 40);
				startPanel.options.get(0).setForeground(new Color(255, 255, 255));
				startPanel.options.get(0).setFont(new Font("Normal", Font.BOLD, 18));
				startPanel.options.get(0).setBackground(new Color(30, 30, 30));
				StartOptionButtonListener sobl0 = new StartOptionButtonListener(this.startPanel,
						startPanel.options.get(0));
				startPanel.options.get(0).addActionListener(sobl0);
				startPanel.add(startPanel.options.get(0));

				startPanel.options.add(new Button("FEMALE"));
				startPanel.options.get(1).setBounds(510, 300, 120, 40);
				startPanel.options.get(1).setForeground(new Color(255, 255, 255));
				startPanel.options.get(1).setFont(new Font("Normal", Font.BOLD, 18));
				startPanel.options.get(1).setBackground(new Color(30, 30, 30));
				StartOptionButtonListener sobl1 = new StartOptionButtonListener(this.startPanel,
						startPanel.options.get(1));
				startPanel.options.get(1).addActionListener(sobl1);
				startPanel.add(startPanel.options.get(1));

			} else {
				startPanel.input.setText("");
			}
		}
	}

	public void keyReleased(KeyEvent e) {

	}

}
