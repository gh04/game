package listeners;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameTextPanel;

public class SkillInfoButtonListener implements ActionListener {
	GameTextPanel textPanel;
	Button skill;

	public SkillInfoButtonListener(GameTextPanel t, Button b) {
		textPanel = t;
		skill = b;
	}

	public void actionPerformed(ActionEvent e) {
		textPanel.setVisible(false);
		textPanel.frame.skillPanel.setVisible(true);
		textPanel.frame.skillPanel.skillInfo(skill.getLabel());
	}

}
