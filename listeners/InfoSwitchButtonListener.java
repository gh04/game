package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameTextPanel;

public class InfoSwitchButtonListener implements ActionListener {
	GameTextPanel textPanel;

	public InfoSwitchButtonListener(GameTextPanel t) {
		textPanel = t;
	}

	public void actionPerformed(ActionEvent e) {
		if (textPanel.infoSwitch.getLabel().equals("STATS")) {
			textPanel.infoSwitch.setLabel("SKILLS");
			textPanel.frame.command.readCommand("stats");
		} else if (textPanel.infoSwitch.getLabel().equals("SKILLS")) {
			textPanel.infoSwitch.setLabel("INFO");
			textPanel.frame.command.readCommand("skills");
		} else if (textPanel.infoSwitch.getLabel().equals("INFO")) {
			textPanel.infoSwitch.setLabel("STATS");
			textPanel.frame.command.readCommand("info");
		}

	}

}
