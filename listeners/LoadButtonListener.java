package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.imageio.ImageIO;

import entity.Enemy;
import frame.GameStartPanel;
import game.Floor;

public class LoadButtonListener implements ActionListener {
	GameStartPanel startPanel;

	public LoadButtonListener(GameStartPanel sp) {
		startPanel = sp;
	}

	public void actionPerformed(ActionEvent e) {
		try {
			FileInputStream fis = new FileInputStream("save.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			startPanel.frame.floorPanel.floor = (Floor) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException | ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		startPanel.frame.floorPanel.setVisible(true);
		startPanel.frame.textPanel.setVisible(true);
		startPanel.frame.floorPanel.floor.hero.changeImage();
		startPanel.frame.floorPanel.floor.boss.changeImage();
		if(startPanel.frame.floorPanel.floor.getFloor()!=8) {
			startPanel.frame.floor.setText("Floor " + startPanel.frame.floorPanel.floor.getFloor());
		} else {
			startPanel.frame.floor.setText("Floor ???");
		}
		startPanel.frame.floorPanel.floor.changeImage();
		for (Enemy enemy : startPanel.frame.floorPanel.floor.enemies) {
			try {
				enemy.setImg(ImageIO.read(new File(enemy.getImgFile())));
			} catch (IOException e1) {
				enemy.setImg(null);
			}
		}
		startPanel.frame.floorPanel.floor.hero.cmd = startPanel.frame.command;
		startPanel.frame.command.readCommand("info");
		startPanel.setVisible(false);
		startPanel.frame.repaint();
	}

}
