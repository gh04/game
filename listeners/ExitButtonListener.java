package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.*;

public class ExitButtonListener implements ActionListener {
	GameFrame frame;

	public ExitButtonListener(GameFrame f) {
		frame = f;
	}

	public void actionPerformed(ActionEvent e) {
		frame.helpPanel.setVisible(false);
		frame.helpPanel.stats.setVisible(true);
		frame.helpPanel.leveling.setVisible(true);
		frame.helpPanel.combatInfo.setVisible(true);
		frame.helpPanel.raceInfo.setVisible(true);
		frame.helpPanel.setHeading("HELP");
		frame.helpPanel.clearText();
		frame.textPanel.setVisible(true);
		frame.floorPanel.setVisible(true);
		frame.help.setVisible(true);
		frame.floor.setText("Floor " + frame.floorPanel.floor.getFloor());
	}

}
