package listeners;

import frame.GameHelpPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class StatsButtonListener implements ActionListener {
	GameHelpPanel helpPanel;
	File help = new File("text/help.txt");
	Scanner sc;

	public StatsButtonListener(GameHelpPanel ghp) {
		helpPanel = ghp;
	}

	public void actionPerformed(ActionEvent e) {
		helpPanel.back.setVisible(true);
		helpPanel.exit.setVisible(false);
		helpPanel.stats.setVisible(false);
		helpPanel.leveling.setVisible(false);
		helpPanel.combatInfo.setVisible(false);
		helpPanel.raceInfo.setVisible(false);
		helpPanel.clearText();
		helpPanel.setHeading("STATS");
		try {
			sc = new Scanner(help);
			for (int i = 0; i < 10; i++) {
				helpPanel.setText(sc.nextLine(), i);
			}
			sc.close();
		} catch (FileNotFoundException e1) {
			helpPanel.setText("Error", 0);
		}
	}

}
