package listeners;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameEndPanel;

public class CloseButtonListener implements ActionListener {
	GameEndPanel endPanel;
	Button opt;

	public CloseButtonListener(GameEndPanel e, Button b) {
		endPanel = e;
		opt = b;
	}

	public void actionPerformed(ActionEvent e) {
		if (opt.getLabel().equals("YES")) {
			endPanel.frame.command.readCommand("save");
		}
		System.exit(0);
	}

}
