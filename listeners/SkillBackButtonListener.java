package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameSkillPanel;

public class SkillBackButtonListener implements ActionListener {
	GameSkillPanel skillPanel;

	public SkillBackButtonListener(GameSkillPanel s) {
		skillPanel = s;
	}

	public void actionPerformed(ActionEvent e) {
		skillPanel.setVisible(false);
		skillPanel.frame.textPanel.setVisible(true);
	}

}
