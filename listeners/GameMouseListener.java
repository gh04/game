package listeners;

import frame.GameStartPanel;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameMouseListener implements MouseListener {
	GameStartPanel startPanel;

	public GameMouseListener(GameStartPanel s) {
		startPanel = s;
	}

	public void mouseClicked(MouseEvent e) {
		Point p = new Point(e.getPoint());
		if (startPanel.start.isVisible() == true&&p.x<60&&p.x>20&&p.y>10&&p.y<40) {
			startPanel.frame.creditsPanel.setVisible(true);
			startPanel.setVisible(false);
		}
	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

}
