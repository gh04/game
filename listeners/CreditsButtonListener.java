package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameCreditsPanel;

public class CreditsButtonListener implements ActionListener{
	GameCreditsPanel creditsPanel;
	
	public CreditsButtonListener(GameCreditsPanel c){
		creditsPanel = c;
	}

	public void actionPerformed(ActionEvent e) {
		creditsPanel.frame.startPanel.setVisible(true);
		creditsPanel.setVisible(false);
	}

}
