package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import frame.GameHelpPanel;

public class CombatInfoButtonListener implements ActionListener {
	GameHelpPanel helpPanel;
	File help = new File("text/help.txt");
	Scanner sc;

	public CombatInfoButtonListener(GameHelpPanel ghp) {
		helpPanel = ghp;
	}

	public void actionPerformed(ActionEvent e) {
		helpPanel.back.setVisible(true);
		helpPanel.exit.setVisible(false);
		helpPanel.stats.setVisible(false);
		helpPanel.leveling.setVisible(false);
		helpPanel.combatInfo.setVisible(false);
		helpPanel.raceInfo.setVisible(false);
		helpPanel.clearText();
		helpPanel.setHeading("COMBAT INFO");
		try {
			int j = 0;
			sc = new Scanner(help);
			for (int i = 0; i < 25; i++) {
				if (i > 17) {
					helpPanel.setText(sc.nextLine(), j);
					j++;
				} else
					sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e1) {
			helpPanel.setText("Error", 0);
		}
	}

}
