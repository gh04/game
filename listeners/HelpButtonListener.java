package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameFrame;

public class HelpButtonListener implements ActionListener {
	GameFrame frame;

	public HelpButtonListener(GameFrame f) {
		frame = f;
	}

	public void actionPerformed(ActionEvent e) {
		frame.command.readCommand("help");
		frame.help.setVisible(false);
		frame.floor.setText("");
	}

}
