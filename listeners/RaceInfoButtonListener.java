package listeners;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import frame.GameHelpPanel;

public class RaceInfoButtonListener implements ActionListener {
	GameHelpPanel helpPanel;
	File rinf = new File("text/races.txt");
	Scanner sc;

	public RaceInfoButtonListener(GameHelpPanel ghp) {
		helpPanel = ghp;
	}

	public void actionPerformed(ActionEvent e) {
		helpPanel.back.setVisible(true);
		helpPanel.exit.setVisible(false);
		helpPanel.stats.setVisible(false);
		helpPanel.leveling.setVisible(false);
		helpPanel.combatInfo.setVisible(false);
		helpPanel.raceInfo.setVisible(false);
		helpPanel.clearText();
		helpPanel.setHeading("RACE INFO");
		switch (helpPanel.frame.floorPanel.floor.hero.getRace()) {
		case "Cambion":
			try {
				sc = new Scanner(rinf);
				for (int i = 0; i < 5; i++) {
					helpPanel.setText(sc.nextLine(), i);
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Lesser Demon":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 10; i++) {
					if (i >= 5) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Demon":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 17; i++) {
					if (i >= 10) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Dhampir":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 25; i++) {
					if (i >= 17) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Ghost":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 33; i++) {
					if (i >= 25) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Ogre":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 41; i++) {
					if (i >= 33) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Ghoul":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 49; i++) {
					if (i >= 41) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Beast":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 57; i++) {
					if (i >= 49) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Majin":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 65; i++) {
					if (i >= 57) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		case "Imp":
			try {
				int j = 0;
				sc = new Scanner(rinf);
				for (int i = 0; i < 73; i++) {
					if (i >= 65) {
						helpPanel.setText(sc.nextLine(), j);
						j++;
					} else
						sc.nextLine();
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				helpPanel.setText("Error", 0);
			}
			break;
		default:
			helpPanel.setText(
					"Description for " + helpPanel.frame.floorPanel.floor.hero.getRace() + " is not avalable yet.", 0);
		}
		helpPanel.label.get(0).setFont(new Font("Normal", Font.PLAIN, 22));
	}

}
