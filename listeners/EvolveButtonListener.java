package listeners;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import entity.Passive;
import entity.Skill;
import frame.GameTextPanel;

public class EvolveButtonListener implements ActionListener {
	GameTextPanel textPanel;
	Button race;

	public EvolveButtonListener(GameTextPanel t, Button b) {
		textPanel = t;
		race = b;
	}

	public void actionPerformed(ActionEvent e) {
		switch (race.getLabel()) {
		case "Dhampir":
			textPanel.frame.floorPanel.floor.hero.setRace("Dhampir"); // sta
			textPanel.frame.floorPanel.floor.hero.setPassive(Passive.Bloodlust);
			textPanel.frame.floorPanel.floor.hero.addSkill(Skill.Blood_Lad.toString().replaceAll("_", " "));
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 6);
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 2);
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 3);
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 1);
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 4);
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 1);
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 5);
			break;
		case "Ghost":
			textPanel.frame.floorPanel.floor.hero.setRace("Ghost"); // dex
			textPanel.frame.floorPanel.floor.hero.setPassive(Passive.Forgotten);
			textPanel.frame.floorPanel.floor.hero.addSkill(Skill.Phase.toString().replaceAll("_", " "));
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 1);
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 1);
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 9);
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 1);
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 4);
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 4);
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 2);
			break;
		case "Ogre":
			textPanel.frame.floorPanel.floor.hero.setRace("Ogre"); // def
			textPanel.frame.floorPanel.floor.hero.setPassive(Passive.Brute);
			textPanel.frame.floorPanel.floor.hero.addSkill(Skill.Smash.toString().replaceAll("_", " "));
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 5);
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 4);
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 0);
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 10);
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 0);
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 3);
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 0);
			break;
		case "Ghoul":
			textPanel.frame.floorPanel.floor.hero.setRace("Ghoul"); // int
			textPanel.frame.floorPanel.floor.hero.setPassive(Passive.Wise);
			textPanel.frame.floorPanel.floor.hero.addSkill(Skill.Arcane_Blast.toString().replaceAll("_", " "));
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 2);
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 2);
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 1);
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 4);
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 6);
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 5);
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 2);
			break;
		case "Beast":
			textPanel.frame.floorPanel.floor.hero.setRace("Beast"); // str
			textPanel.frame.floorPanel.floor.hero.setPassive(Passive.Ferocious);
			textPanel.frame.floorPanel.floor.hero.addSkill(Skill.Berserk.toString().replaceAll("_", " "));
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 4);
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 7);
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 5);
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 1);
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 0);
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 3);
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 2);
			break;
		case "Majin":
			textPanel.frame.floorPanel.floor.hero.setRace("Majin"); // mas
			textPanel.frame.floorPanel.floor.hero.setPassive(Passive.Unyielding);
			textPanel.frame.floorPanel.floor.hero.addSkill(Skill.True_Slash.toString().replaceAll("_", " "));
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 2);
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 4);
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 4);
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 0);
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 2);
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 9);
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 3);
			break;
		case "Imp":
			textPanel.frame.floorPanel.floor.hero.setRace("Imp"); // cha
			textPanel.frame.floorPanel.floor.hero.setPassive(Passive.Lustful);
			textPanel.frame.floorPanel.floor.hero.addSkill(Skill.Charm.toString().replaceAll("_", " "));
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 4);
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 0);
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 2);
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 1);
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 6);
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 2);
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 7);
			break;
		}
		textPanel.frame.floorPanel.floor.hero.setMaxHP(6 + textPanel.frame.floorPanel.floor.hero.getSTA());
		textPanel.frame.floorPanel.floor.hero.setCurrentHP(textPanel.frame.floorPanel.floor.hero.getMaxHP());
		textPanel.clearButtons();
		textPanel.frame.floorPanel.floor.hero.changeImage();
		textPanel.infoSwitch.setVisible(true);
		textPanel.infoSwitch.setLabel("STATS");
		textPanel.frame.command.readCommand("info");
	}

}
