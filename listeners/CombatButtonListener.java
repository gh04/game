package listeners;

import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameTextPanel;

public class CombatButtonListener implements ActionListener {
	GameTextPanel textPanel;
	Button skill;

	public CombatButtonListener(GameTextPanel p, Button b) {
		textPanel = p;
		skill = b;
	}

	public void actionPerformed(ActionEvent e) {
		textPanel.frame.combatPanel.repaint();
		textPanel.frame.command.combat.setSkill(skill.getLabel());
		if(!skill.getLabel().equals("Hit")) {
			skill.setEnabled(false);
			skill.setBackground(new Color(50, 30, 30));
		}
		if (textPanel.getEnemyType().equals("enemy"))
			textPanel.frame.command.combat.enemyFight();
		else if (textPanel.getEnemyType().equals("boss"))
			textPanel.frame.command.combat.bossFight();
	}

}
