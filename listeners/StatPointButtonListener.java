package listeners;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.GameTextPanel;

public class StatPointButtonListener implements ActionListener {
	GameTextPanel textPanel;
	Button stat;

	public StatPointButtonListener(GameTextPanel t, Button s) {
		textPanel = t;
		stat = s;
	}

	public void actionPerformed(ActionEvent e) {
		int statNu = (stat.getY() - 85) / 30;
		switch (statNu) {
		case 0:
			textPanel.frame.floorPanel.floor.hero.setSTA(textPanel.frame.floorPanel.floor.hero.getSTA() + 1);
			textPanel.frame.floorPanel.floor.hero.setSP(textPanel.frame.floorPanel.floor.hero.getSP() - 1);
			textPanel.frame.floorPanel.floor.hero.setMaxHP(6 + textPanel.frame.floorPanel.floor.hero.getSTA());
			textPanel.frame.floorPanel.floor.hero.setCurrentHP(textPanel.frame.floorPanel.floor.hero.getCurrentHP()+1);
			break;
		case 1:
			textPanel.frame.floorPanel.floor.hero.setSTR(textPanel.frame.floorPanel.floor.hero.getSTR() + 1);
			textPanel.frame.floorPanel.floor.hero.setSP(textPanel.frame.floorPanel.floor.hero.getSP() - 1);
			break;
		case 2:
			textPanel.frame.floorPanel.floor.hero.setDEX(textPanel.frame.floorPanel.floor.hero.getDEX() + 1);
			textPanel.frame.floorPanel.floor.hero.setSP(textPanel.frame.floorPanel.floor.hero.getSP() - 1);
			break;
		case 3:
			textPanel.frame.floorPanel.floor.hero.setDEF(textPanel.frame.floorPanel.floor.hero.getDEF() + 1);
			textPanel.frame.floorPanel.floor.hero.setSP(textPanel.frame.floorPanel.floor.hero.getSP() - 1);
			break;
		case 4:
			textPanel.frame.floorPanel.floor.hero.setINT(textPanel.frame.floorPanel.floor.hero.getINT() + 1);
			textPanel.frame.floorPanel.floor.hero.setSP(textPanel.frame.floorPanel.floor.hero.getSP() - 1);
			break;
		case 5:
			textPanel.frame.floorPanel.floor.hero.setMAS(textPanel.frame.floorPanel.floor.hero.getMAS() + 1);
			textPanel.frame.floorPanel.floor.hero.setSP(textPanel.frame.floorPanel.floor.hero.getSP() - 1);
			break;
		case 6:
			textPanel.frame.floorPanel.floor.hero.setCHA(textPanel.frame.floorPanel.floor.hero.getCHA() + 1);
			textPanel.frame.floorPanel.floor.hero.setSP(textPanel.frame.floorPanel.floor.hero.getSP() - 1);
			break;
		}
		if (textPanel.frame.floorPanel.floor.hero.getSP() == 0) {
			textPanel.clearButtons();
		}
		textPanel.frame.command.readCommand("stats");
	}

}
