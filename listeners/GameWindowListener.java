package listeners;

import frame.GameFrame;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class GameWindowListener implements WindowListener {
	GameFrame frame;

	public GameWindowListener(GameFrame f) {
		frame = f;
	}

	public void windowOpened(WindowEvent e) {

	}

	public void windowClosing(WindowEvent e) {
		if(frame.startPanel.isVisible()==true||frame.endPanel.isVisible()==true||frame.creditsPanel.isVisible()==true) {
			System.exit(0);
		}
		else {
			frame.endPanel.gameClose();
		}
	}

	public void windowClosed(WindowEvent e) {

	}

	public void windowIconified(WindowEvent e) {

	}

	public void windowDeiconified(WindowEvent e) {

	}

	public void windowActivated(WindowEvent e) {

	}

	public void windowDeactivated(WindowEvent e) {

	}

}
